﻿using Intuit.Ipp.Core;
using Intuit.Ipp.Data;
using Intuit.Ipp.Security;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IntuitApp
{
    public partial class Form1 : Form
    {
        private String sep1 = "../";
        private String sep2 = "../../../";
        private String filePath = Environment.CurrentDirectory + "../" + "KeyFile.txt";
        Intuit.Ipp.DataService.DataService service;
        IntuitApp.ClassFiles.IntuitClass runinstance;
        private List<Bill> allBilles = new List<Bill>();
        private List<Customer> allcustomer = new List<Customer>();
        private List<Employee> allemployee = new List<Employee>();
        private List<Invoice> allinvoice = new List<Invoice>();
        private List<Item> allitem = new List<Item>();
        private List<PurchaseOrder> allpurchaseorder = new List<PurchaseOrder>();


        public Form1()
        {
            InitializeComponent();
            filePath = Environment.CurrentDirectory + sep1 + "KeyFile.txt";
            if (File.Exists(filePath))
            {
                runinstance = new ClassFiles.IntuitClass(filePath);
            }
            else
            {
                filePath = Environment.CurrentDirectory + sep2 + "KeyFile.txt";
                runinstance = new ClassFiles.IntuitClass(filePath);
            }
            service = runinstance.service;
        }
        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void ApplyCustomer(Intuit.Ipp.Data.Customer entity)
        {
            tb_r_customer_displayname.Text = "";
            tb_r_customer_familyname.Text = "";
            tb_r_customer_givenname.Text = "";
            tb_r_customer_notes.Text = "";

            if (entity != null)
            {
                if (entity.DisplayName != null) tb_r_customer_displayname.Text = entity.DisplayName.ToString();
                if (entity.FamilyName != null) tb_r_customer_familyname.Text = entity.FamilyName.ToString();
                if (entity.GivenName != null) tb_r_customer_givenname.Text = entity.GivenName.ToString();
                if (entity.Notes != null) tb_r_customer_notes.Text = entity.Notes.ToString();
            }
        }
        private void ApplyInvoice(Intuit.Ipp.Data.Invoice entity)
        {
            if (entity != null)
            {

            }
        }
        private void ApplyBill(Intuit.Ipp.Data.Bill entity)
        {
            tb_r_bill_totAmount.Text = "";
            tb_r_bill_balance.Text = "";
            tb_r_bill_vendorRef.Text = "";
            if (entity != null)
            {
                tb_r_bill_totAmount.Text = entity.TotalAmt.ToString();
                tb_r_bill_balance.Text = entity.Balance.ToString();
                if (entity.VendorRef != null) tb_r_bill_vendorRef.Text = entity.VendorRef.Value ?? "";

            }
        }
        private void ApplyPurchaseOrder(Intuit.Ipp.Data.PurchaseOrder entity)
        {
            if (entity != null)
            {
                readPurchase_result.Visible = true;
            }
        }
        private void ApplyEmployee(Intuit.Ipp.Data.Employee entity)
        {
            tb_r_employee_givenname.Text = "";
            tb_r_employee_familyname.Text = "";
            tb_r_employee_checkname.Text = "";
            if (entity != null)
            {
                tb_r_employee_givenname.Text = entity.GivenName ?? "";
                tb_r_employee_familyname.Text = entity.FamilyName ?? "";
                tb_r_employee_checkname.Text = entity.PrintOnCheckName ?? "";
            }
        }
        private void ApplyItem(Intuit.Ipp.Data.Item entity)
        {
            tb_r_item_assest.Text = "";
            tb_r_item_expense.Text = "";
            tb_r_item_income.Text = "";
            tb_r_item_name.Text = "";
            if (entity != null)
            {
                if (entity.AssetAccountRef != null) tb_r_item_assest.Text = entity.AssetAccountRef.Value ?? "";
                if (entity.ExpenseAccountRef != null) tb_r_item_expense.Text = entity.ExpenseAccountRef.Value ?? "";
                if (entity.IncomeAccountRef != null) tb_r_item_income.Text = entity.IncomeAccountRef.Value ?? "";
                tb_r_item_name.Text = entity.Name ?? "";
            }
        }


        /// <summary>
        /// DeleteBill
        /// </summary>
        private void button7_Click(object sender, EventArgs e)
        {
            if (runinstance.DeleteBill(tb_d_bill_id.Text))
            {
                MessageBox.Show("Deleted Successfully.");
            }
            else
            {
                MessageBox.Show("Error Occured.");
            }
        }
        /// <summary>
        /// DeleteInvoice
        /// </summary>
        private void button10_Click(object sender, EventArgs e)
        {
            if (runinstance.DeleteInvoice(tb_d_invoice_id.Text))
            {
                MessageBox.Show("Deleted Successfully.");
            }
            else
            {
                MessageBox.Show("Error Occured.");
            }
        }
        /// <summary>
        /// DeletePurchaseOrder
        /// </summary>
        private void button12_Click(object sender, EventArgs e)
        {
            if (runinstance.DeletePurchaseOrder(tb_d_purchase_id.Text))
            {
                MessageBox.Show("Deleted Successfully.");
            }
            else
            {
                MessageBox.Show("Error Occured.");
            }
        }

        /// <summary>
        /// CreateCutomer
        /// </summary>
        private void button2_Click(object sender, EventArgs e)
        {
            String result = runinstance.CreateCutomer(tb_c_customer_displayname.Text, tb_c_customer_givenname.Text, tb_c_customer_familyname.Text, tb_c_customer_note.Text);
            if (result.Equals(""))
            {
                MessageBox.Show("Error Occured.");
            }
            else
            {
                MessageBox.Show("Create Successfully and Id is " + result);
            }

        }
        /// <summary>
        /// CreateEmployee
        /// </summary>
        private void button3_Click(object sender, EventArgs e)
        {
            String result = runinstance.CreateEmployee(tb_c_employee_givenname.Text, tb_c_employee_familyname.Text);
            if (result.Equals(""))
            {
                MessageBox.Show("Error Occured.");
            }
            else
            {
                MessageBox.Show("Create Successfully and Id is " + result);
            }
        }
        /// <summary>
        /// CreateItem
        /// </summary>
        private void button5_Click(object sender, EventArgs e)
        {
            String result = runinstance.CreateItem(tb_c_item_name.Text, tb_c_item_income.Text, tb_c_item_expense.Text, tb_c_item_assest.Text, dt_c_item_date.Value);
            if (result.Equals(""))
            {
                MessageBox.Show("Error Occured.");
            }
            else
            {
                MessageBox.Show("Create Successfully and Id is " + result);
            }
        }
        /// <summary>
        /// Create Bill 
        /// </summary>
        private void button1_Click(object sender, EventArgs e)
        {
            Intuit.Ipp.Data.Bill newBill = new Intuit.Ipp.Data.Bill();
            Intuit.Ipp.Data.Line newLine = new Intuit.Ipp.Data.Line
            {
                Id = "1",
                Amount = Convert.ToDecimal(tb_c_bill_amount.Text),
                DetailType = Intuit.Ipp.Data.LineDetailTypeEnum.AccountBasedExpenseLineDetail
            };
            Intuit.Ipp.Data.Line[] lines = { newLine };
            newBill.Line = lines;
            ReferenceType refe = new Intuit.Ipp.Data.ReferenceType();
            refe.Value = tb_c_bill_vendorRefVal.Text;
            newBill.VendorRef = refe;
            try
            {
                Bill getBill = service.Add<Bill>(newBill);
                MessageBox.Show("Bill Created and Id is :" + getBill.Id);
            }
            catch (Intuit.Ipp.Exception.IdsException ee)
            {
                MessageBox.Show(ee.ErrorCode + "\n" + ee.Message);
            }

        }
        /// <summary>
        /// Create Invoice 
        /// </summary>
        private void button4_Click(object sender, EventArgs e)
        {
            Intuit.Ipp.Data.Invoice newBill = new Intuit.Ipp.Data.Invoice();
            Intuit.Ipp.Data.Line newLine = new Intuit.Ipp.Data.Line
            {
                Amount = Convert.ToDecimal(tb_c_invoice_amount.Text),
                DetailType = Intuit.Ipp.Data.LineDetailTypeEnum.SalesItemLineDetail
            };
            Intuit.Ipp.Data.Line[] lines = { newLine };
            newBill.Line = lines;
            ReferenceType type = new ReferenceType();
            type.Value = tb_c_invoice_customerref.Text;
            newBill.CustomerRef = type;
            try
            {
                Invoice getBill = service.Add<Invoice>(newBill);
                MessageBox.Show("Invoice Created and Id is :" + getBill.Id);
            }
            catch (Intuit.Ipp.Exception.IdsException ee)
            {
                MessageBox.Show(ee.ErrorCode + "\n" + ee.Message);
            }
        }
        /// <summary>
        /// Create Purchase Order 
        /// </summary>
        private void button6_Click(object sender, EventArgs e)
        {
            Intuit.Ipp.Data.PurchaseOrder item = new PurchaseOrder();
            ReferenceType ap = new ReferenceType();
            ReferenceType vendor = new ReferenceType();
            ReferenceType itemref = new ReferenceType();
            ReferenceType custref = new ReferenceType();
            custref.Value = tb_c_purchase_customer.Text;
            itemref.Value = tb_c_purchase_itemref.Text;
            ap.Value = tb_c_purchase_apaccount.Text;
            vendor.Value = tb_c_purchase_vendorref.Text;
            item.APAccountRef = ap;
            item.VendorRef = vendor;
            Line line = new Line();
            line.Id = "1";
            line.Amount = Convert.ToDecimal(tb_c_purchase_amount.Text);
            line.DetailType = LineDetailTypeEnum.ItemBasedExpenseLineDetail;
            ItemBasedExpenseLineDetail detail = new ItemBasedExpenseLineDetail();
            if (radio_c_purchase_billable.Checked)
            {
                detail.BillableStatus = BillableStatusEnum.Billable;
            }
            if (radio_c_purchase_hasbilled.Checked)
            {
                detail.BillableStatus = BillableStatusEnum.HasBeenBilled;
            }
            if (radio_c_purchase_billable.Checked)
            {
                detail.BillableStatus = BillableStatusEnum.NotBillable;
            }
            detail.ItemRef = itemref;
            detail.Qty = Convert.ToDecimal(tb_c_purchase_quantity.Text);
            detail.CustomerRef = custref;
            line.AnyIntuitObject = detail;
            Line[] allLines = { line };
            item.Line = allLines;
            try
            {
                PurchaseOrder getBill = service.Add<PurchaseOrder>(item);
                MessageBox.Show("Purchase Order Created and Id is :" + getBill.Id);
            }
            catch (Intuit.Ipp.Exception.IdsException ee)
            {
                MessageBox.Show(ee.ErrorCode + "\n" + ee.Message);
            }
        }

        /// <summary>
        /// GetAllBills
        /// </summary>
        private void button_showAll_Click(object sender, EventArgs e)
        {
            allBilles = runinstance.GetAllBills();
            listBox_bill.Items.Clear();
            foreach (var item in allBilles)
            {
                listBox_bill.Items.Add(item.Id);
            }
        }
        private void showAll_Employee_Click(object sender, EventArgs e)
        {
            allemployee = runinstance.GetAllEmployee();
            listBox_employee.Items.Clear();
            foreach (var item in allemployee)
            {
                listBox_employee.Items.Add(item.Id);
            }
        }
        private void showAll_Purchase_Click(object sender, EventArgs e)
        {
            allpurchaseorder = runinstance.GetAllPurchaseOrder();
            listBox_purchase.Items.Clear();
            foreach (var item in allpurchaseorder)
            {
                listBox_purchase.Items.Add(item.Id);
            }
        }
        private void showAll_Invoice_Click(object sender, EventArgs e)
        {
            allinvoice = runinstance.GetAllInvoices();
            listBox_invoice.Items.Clear();
            foreach (var item in allinvoice)
            {
                listBox_invoice.Items.Add(item.Id);
            }
        }
        private void showAll_Items_Click(object sender, EventArgs e)
        {
            allitem = runinstance.GetAllItems();
            listBox_Item.Items.Clear();
            foreach (var item in allitem)
            {
                listBox_Item.Items.Add(item.Id);
            }
        }

        /// <summary>
        /// Update_Bill
        /// </summary>
        private void button23_Click(object sender, EventArgs e)
        {
            Bill cm = allBilles.Find(c => c.Id == listBox_bill.Text);
            cm.TotalAmt = Convert.ToDecimal(tb_r_bill_totAmount.Text);
            cm.Balance = Convert.ToDecimal(tb_r_bill_balance.Text);
            cm.VendorRef.Value = tb_r_bill_vendorRef.Text;
            if (runinstance.Update_Bill(cm))
            {
                MessageBox.Show("Updated.");
            }
            else
            {
                MessageBox.Show("Error Occured.");
            }
        }
        private void Customer_Update_Click(object sender, EventArgs e)
        {
            Customer cm = allcustomer.Find(c => c.Id == listBox_customer.Text);
            cm.GivenName = tb_r_customer_givenname.Text;
            cm.FamilyName = tb_r_customer_familyname.Text;
            cm.DisplayName = tb_r_customer_displayname.Text;
            cm.Notes = tb_r_customer_notes.Text;
            if (runinstance.Update_Customer(cm))
            {
                MessageBox.Show("Updated.");
            }
            else
            {
                MessageBox.Show("Error Occured.");
            }

        }
        private void update_employee_Click(object sender, EventArgs e)
        {
            Employee cm = allemployee.Find(c => c.Id == listBox_employee.Text);
            cm.GivenName = tb_r_employee_givenname.Text;
            cm.FamilyName = tb_r_employee_familyname.Text;
            cm.PrintOnCheckName = tb_r_employee_checkname.Text;
            if (runinstance.Update_Employee(cm))
            {
                MessageBox.Show("Updated.");
            }
            else
            {
                MessageBox.Show("Error Occured.");
            }
        }
        private void update_item_Click(object sender, EventArgs e)
        {
            Item cm = allitem.Find(c => c.Id == listBox_Item.Text);
            cm.Name = tb_r_item_name.Text;
            cm.IncomeAccountRef.Value = tb_r_item_income.Text;
            cm.ExpenseAccountRef.Value = tb_r_item_expense.Text;
            cm.AssetAccountRef.Value = tb_r_item_assest.Text;
            if (runinstance.Update_Item(cm))
            {
                MessageBox.Show("Updated.");
            }
            else
            {
                MessageBox.Show("Error Occured.");
            }
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            allcustomer = runinstance.GetAllCustomers();
            listBox_customer.Items.Clear();
            foreach (var item in allcustomer)
            {
                listBox_customer.Items.Add(item.Id);
            }
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            ApplyBill(allBilles.Find(c => c.Id == listBox_bill.Text));
        }
        private void listBox_employee_SelectedIndexChanged(object sender, EventArgs e)
        {
            ApplyEmployee(allemployee.Find(c => c.Id == listBox_employee.Text));
        }
        private void listBox_purchase_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        private void listBox_invoice_SelectedIndexChanged(object sender, EventArgs e)
        {
            ApplyInvoice(allinvoice.Find(c => c.Id == listBox_invoice.Text));
        }
        private void listBox_Item_SelectedIndexChanged(object sender, EventArgs e)
        {
            ApplyItem(allitem.Find(c => c.Id == listBox_Item.Text));
        }
        private void listBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            ApplyCustomer(allcustomer.Find(c => c.Id == listBox_customer.Text));
        }

        private void bt_b_bill_execute_Click(object sender, EventArgs e)
        {
            allBilles = runinstance.GetAllBills(tb_b_bill_query.Text.ToString());
            listBox_bill.Items.Clear();
            foreach (var item in allBilles)
            {
                listBox_bill.Items.Add(item.Id);
            }
        }

        private void bt_b_purchase_execute_Click(object sender, EventArgs e)
        {
            allpurchaseorder = runinstance.GetAllPurchaseOrder(tb_b_purchase_query.Text.ToString());
            listBox_purchase.Items.Clear();
            foreach (var item in allpurchaseorder)
            {
                listBox_purchase.Items.Add(item.Id);
            }
        }

        private void bt_b_item_execute_Click(object sender, EventArgs e)
        {
            allitem = runinstance.GetAllItems(tb_b_item_query.Text.ToString());
            listBox_Item.Items.Clear();
            foreach (var item in allitem)
            {
                listBox_Item.Items.Add(item.Id);
            }
        }

        private void bt_b_invoice_execute_Click(object sender, EventArgs e)
        {
            allinvoice = runinstance.GetAllInvoices(tb_b_invoice_query.Text.ToString());
            listBox_invoice.Items.Clear();
            foreach (var item in allinvoice)
            {
                listBox_invoice.Items.Add(item.Id);
            }
        }

        private void bt_b_employee_execute_Click(object sender, EventArgs e)
        {
            allemployee = runinstance.GetAllEmployee(tb_b_employee_query.Text.ToString());
            listBox_employee.Items.Clear();
            foreach (var item in allemployee)
            {
                listBox_employee.Items.Add(item.Id);
            }
        }

        private void bt_b_customer_execute_Click(object sender, EventArgs e)
        {
            allcustomer = runinstance.GetAllCustomers(tb_b_customer_query.Text.ToString());
            listBox_customer.Items.Clear();
            foreach (var item in allcustomer)
            {
                listBox_customer.Items.Add(item.Id);
            }
        }
    }
}
