﻿namespace IntuitApp
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.readPurchase_result = new System.Windows.Forms.TabControl();
            this.tab_Bill = new System.Windows.Forms.TabPage();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.bt_c_bill_create = new System.Windows.Forms.Button();
            this.label66 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.tb_c_bill_amount = new System.Windows.Forms.TextBox();
            this.tb_c_bill_vendorRefVal = new System.Windows.Forms.TextBox();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.panel9 = new System.Windows.Forms.Panel();
            this.bt_r_bill_update = new System.Windows.Forms.Button();
            this.txtMatchingField = new System.Windows.Forms.TextBox();
            this.tb_r_bill_vendorRef = new System.Windows.Forms.TextBox();
            this.tb_r_bill_balance = new System.Windows.Forms.TextBox();
            this.tb_r_bill_totAmount = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.listBox_bill = new System.Windows.Forms.ListBox();
            this.label141 = new System.Windows.Forms.Label();
            this.label139 = new System.Windows.Forms.Label();
            this.label138 = new System.Windows.Forms.Label();
            this.toolStrip_bill = new System.Windows.Forms.ToolStrip();
            this.bt_showall_bill = new System.Windows.Forms.ToolStripButton();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.bt_d_bill_delete = new System.Windows.Forms.Button();
            this.label110 = new System.Windows.Forms.Label();
            this.tb_d_bill_id = new System.Windows.Forms.TextBox();
            this.tab_Customer = new System.Windows.Forms.TabPage();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.label78 = new System.Windows.Forms.Label();
            this.tb_c_customer_note = new System.Windows.Forms.TextBox();
            this.label64 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.tb_c_customer_displayname = new System.Windows.Forms.TextBox();
            this.tb_c_customer_familyname = new System.Windows.Forms.TextBox();
            this.tb_c_customer_givenname = new System.Windows.Forms.TextBox();
            this.bt_c_customer_create = new System.Windows.Forms.Button();
            this.label62 = new System.Windows.Forms.Label();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.bt_r_customer_update = new System.Windows.Forms.Button();
            this.tb_r_customer_notes = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.tb_r_customer_displayname = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tb_r_customer_givenname = new System.Windows.Forms.TextBox();
            this.listBox_customer = new System.Windows.Forms.ListBox();
            this.tb_r_customer_familyname = new System.Windows.Forms.TextBox();
            this.toolStrip_customer = new System.Windows.Forms.ToolStrip();
            this.bt_showall_customer = new System.Windows.Forms.ToolStripButton();
            this.tab_Employee = new System.Windows.Forms.TabPage();
            this.tabControl3 = new System.Windows.Forms.TabControl();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.bt_c_employee_create = new System.Windows.Forms.Button();
            this.label72 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.tb_c_employee_givenname = new System.Windows.Forms.TextBox();
            this.tb_c_employee_familyname = new System.Windows.Forms.TextBox();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.panel8 = new System.Windows.Forms.Panel();
            this.tb_r_employee_checkname = new System.Windows.Forms.TextBox();
            this.tb_r_employee_familyname = new System.Windows.Forms.TextBox();
            this.tb_r_employee_givenname = new System.Windows.Forms.TextBox();
            this.bt_r_employee_update = new System.Windows.Forms.Button();
            this.label29 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.listBox_employee = new System.Windows.Forms.ListBox();
            this.toolStrip_employee = new System.Windows.Forms.ToolStrip();
            this.bt_showall_employee = new System.Windows.Forms.ToolStripButton();
            this.tab_Invoice = new System.Windows.Forms.TabPage();
            this.tabControl6 = new System.Windows.Forms.TabControl();
            this.tabPage14 = new System.Windows.Forms.TabPage();
            this.bt_c_invoice_create = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.tb_c_invoice_amount = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tb_c_invoice_itemref = new System.Windows.Forms.TextBox();
            this.tb_c_invoice_customerref = new System.Windows.Forms.TextBox();
            this.tabPage15 = new System.Windows.Forms.TabPage();
            this.bt_r_invoice_update = new System.Windows.Forms.Button();
            this.label36 = new System.Windows.Forms.Label();
            this.listBox_invoice = new System.Windows.Forms.ListBox();
            this.label37 = new System.Windows.Forms.Label();
            this.toolstrip = new System.Windows.Forms.ToolStrip();
            this.bt_showall_invoice = new System.Windows.Forms.ToolStripButton();
            this.label38 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.tabPage16 = new System.Windows.Forms.TabPage();
            this.tb_d_invoice_id = new System.Windows.Forms.TextBox();
            this.bt_d_invoice_delete = new System.Windows.Forms.Button();
            this.label116 = new System.Windows.Forms.Label();
            this.tab_Item = new System.Windows.Forms.TabPage();
            this.tabControl4 = new System.Windows.Forms.TabControl();
            this.tabPage9 = new System.Windows.Forms.TabPage();
            this.dt_c_item_date = new System.Windows.Forms.DateTimePicker();
            this.bt_c_item_create = new System.Windows.Forms.Button();
            this.label30 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.tb_c_item_name = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.tb_c_item_assest = new System.Windows.Forms.TextBox();
            this.tb_c_item_expense = new System.Windows.Forms.TextBox();
            this.tb_c_item_income = new System.Windows.Forms.TextBox();
            this.tabPage10 = new System.Windows.Forms.TabPage();
            this.panel7 = new System.Windows.Forms.Panel();
            this.tb_r_item_assest = new System.Windows.Forms.TextBox();
            this.tb_r_item_expense = new System.Windows.Forms.TextBox();
            this.tb_r_item_income = new System.Windows.Forms.TextBox();
            this.tb_r_item_name = new System.Windows.Forms.TextBox();
            this.bt_r_item_update = new System.Windows.Forms.Button();
            this.label75 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.label79 = new System.Windows.Forms.Label();
            this.label80 = new System.Windows.Forms.Label();
            this.listBox_Item = new System.Windows.Forms.ListBox();
            this.toolStrip_item = new System.Windows.Forms.ToolStrip();
            this.bt_showall_item = new System.Windows.Forms.ToolStripButton();
            this.tab_PurchaseOrder = new System.Windows.Forms.TabPage();
            this.tabControl5 = new System.Windows.Forms.TabControl();
            this.tabPage11 = new System.Windows.Forms.TabPage();
            this.label108 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.tb_c_purchase_customer = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.label107 = new System.Windows.Forms.Label();
            this.tb_c_purchase_vendorref = new System.Windows.Forms.TextBox();
            this.tb_c_purchase_quantity = new System.Windows.Forms.TextBox();
            this.tb_c_purchase_apaccount = new System.Windows.Forms.TextBox();
            this.radio_c_purchase_notbillable = new System.Windows.Forms.RadioButton();
            this.label28 = new System.Windows.Forms.Label();
            this.radio_c_purchase_hasbilled = new System.Windows.Forms.RadioButton();
            this.bt_c_purchase_create = new System.Windows.Forms.Button();
            this.radio_c_purchase_billable = new System.Windows.Forms.RadioButton();
            this.tb_c_purchase_amount = new System.Windows.Forms.TextBox();
            this.label76 = new System.Windows.Forms.Label();
            this.tb_c_purchase_itemref = new System.Windows.Forms.TextBox();
            this.tb_c_purchase_unitprice = new System.Windows.Forms.TextBox();
            this.label74 = new System.Windows.Forms.Label();
            this.tabPage12 = new System.Windows.Forms.TabPage();
            this.bt_r_purchase_update = new System.Windows.Forms.Button();
            this.label82 = new System.Windows.Forms.Label();
            this.listBox_purchase = new System.Windows.Forms.ListBox();
            this.label83 = new System.Windows.Forms.Label();
            this.label84 = new System.Windows.Forms.Label();
            this.toolStrip_purchase = new System.Windows.Forms.ToolStrip();
            this.bt_showall_purc = new System.Windows.Forms.ToolStripButton();
            this.label85 = new System.Windows.Forms.Label();
            this.label103 = new System.Windows.Forms.Label();
            this.label86 = new System.Windows.Forms.Label();
            this.label102 = new System.Windows.Forms.Label();
            this.label87 = new System.Windows.Forms.Label();
            this.label101 = new System.Windows.Forms.Label();
            this.label88 = new System.Windows.Forms.Label();
            this.label100 = new System.Windows.Forms.Label();
            this.label89 = new System.Windows.Forms.Label();
            this.label99 = new System.Windows.Forms.Label();
            this.label90 = new System.Windows.Forms.Label();
            this.label98 = new System.Windows.Forms.Label();
            this.label91 = new System.Windows.Forms.Label();
            this.label97 = new System.Windows.Forms.Label();
            this.label92 = new System.Windows.Forms.Label();
            this.label96 = new System.Windows.Forms.Label();
            this.label93 = new System.Windows.Forms.Label();
            this.label95 = new System.Windows.Forms.Label();
            this.label94 = new System.Windows.Forms.Label();
            this.tabPage13 = new System.Windows.Forms.TabPage();
            this.tb_d_purchase_id = new System.Windows.Forms.TextBox();
            this.label118 = new System.Windows.Forms.Label();
            this.bt_d_purchase_delete = new System.Windows.Forms.Button();
            this.tb_b_bill_query = new System.Windows.Forms.ToolStripTextBox();
            this.bt_b_bill_execute = new System.Windows.Forms.ToolStripButton();
            this.tb_b_customer_query = new System.Windows.Forms.ToolStripTextBox();
            this.bt_b_customer_execute = new System.Windows.Forms.ToolStripButton();
            this.tb_b_employee_query = new System.Windows.Forms.ToolStripTextBox();
            this.bt_b_employee_execute = new System.Windows.Forms.ToolStripButton();
            this.tb_b_invoice_query = new System.Windows.Forms.ToolStripTextBox();
            this.bt_b_invoice_execute = new System.Windows.Forms.ToolStripButton();
            this.tb_b_item_query = new System.Windows.Forms.ToolStripTextBox();
            this.bt_b_item_execute = new System.Windows.Forms.ToolStripButton();
            this.tb_b_purchase_query = new System.Windows.Forms.ToolStripTextBox();
            this.bt_b_purchase_execute = new System.Windows.Forms.ToolStripButton();
            this.readPurchase_result.SuspendLayout();
            this.tab_Bill.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.panel9.SuspendLayout();
            this.toolStrip_bill.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.tab_Customer.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.panel1.SuspendLayout();
            this.toolStrip_customer.SuspendLayout();
            this.tab_Employee.SuspendLayout();
            this.tabControl3.SuspendLayout();
            this.tabPage7.SuspendLayout();
            this.tabPage8.SuspendLayout();
            this.panel8.SuspendLayout();
            this.toolStrip_employee.SuspendLayout();
            this.tab_Invoice.SuspendLayout();
            this.tabControl6.SuspendLayout();
            this.tabPage14.SuspendLayout();
            this.tabPage15.SuspendLayout();
            this.toolstrip.SuspendLayout();
            this.tabPage16.SuspendLayout();
            this.tab_Item.SuspendLayout();
            this.tabControl4.SuspendLayout();
            this.tabPage9.SuspendLayout();
            this.tabPage10.SuspendLayout();
            this.panel7.SuspendLayout();
            this.toolStrip_item.SuspendLayout();
            this.tab_PurchaseOrder.SuspendLayout();
            this.tabControl5.SuspendLayout();
            this.tabPage11.SuspendLayout();
            this.tabPage12.SuspendLayout();
            this.toolStrip_purchase.SuspendLayout();
            this.tabPage13.SuspendLayout();
            this.SuspendLayout();
            // 
            // readPurchase_result
            // 
            this.readPurchase_result.Controls.Add(this.tab_Bill);
            this.readPurchase_result.Controls.Add(this.tab_Customer);
            this.readPurchase_result.Controls.Add(this.tab_Employee);
            this.readPurchase_result.Controls.Add(this.tab_Invoice);
            this.readPurchase_result.Controls.Add(this.tab_Item);
            this.readPurchase_result.Controls.Add(this.tab_PurchaseOrder);
            this.readPurchase_result.Dock = System.Windows.Forms.DockStyle.Fill;
            this.readPurchase_result.ItemSize = new System.Drawing.Size(19, 25);
            this.readPurchase_result.Location = new System.Drawing.Point(0, 0);
            this.readPurchase_result.Multiline = true;
            this.readPurchase_result.Name = "readPurchase_result";
            this.readPurchase_result.SelectedIndex = 0;
            this.readPurchase_result.ShowToolTips = true;
            this.readPurchase_result.Size = new System.Drawing.Size(759, 500);
            this.readPurchase_result.TabIndex = 0;
            // 
            // tab_Bill
            // 
            this.tab_Bill.Controls.Add(this.tabControl1);
            this.tab_Bill.Location = new System.Drawing.Point(4, 29);
            this.tab_Bill.Name = "tab_Bill";
            this.tab_Bill.Padding = new System.Windows.Forms.Padding(3);
            this.tab_Bill.Size = new System.Drawing.Size(751, 467);
            this.tab_Bill.TabIndex = 0;
            this.tab_Bill.Text = "Bill";
            this.tab_Bill.UseVisualStyleBackColor = true;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(3, 3);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(745, 461);
            this.tabControl1.TabIndex = 1;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.bt_c_bill_create);
            this.tabPage2.Controls.Add(this.label66);
            this.tabPage2.Controls.Add(this.label60);
            this.tabPage2.Controls.Add(this.tb_c_bill_amount);
            this.tabPage2.Controls.Add(this.tb_c_bill_vendorRefVal);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(737, 435);
            this.tabPage2.TabIndex = 0;
            this.tabPage2.Text = "Create";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // bt_c_bill_create
            // 
            this.bt_c_bill_create.Location = new System.Drawing.Point(72, 98);
            this.bt_c_bill_create.Name = "bt_c_bill_create";
            this.bt_c_bill_create.Size = new System.Drawing.Size(75, 23);
            this.bt_c_bill_create.TabIndex = 4;
            this.bt_c_bill_create.Text = "Create";
            this.bt_c_bill_create.UseVisualStyleBackColor = true;
            this.bt_c_bill_create.Click += new System.EventHandler(this.button1_Click);
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(17, 41);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(94, 13);
            this.label66.TabIndex = 3;
            this.label66.Text = "Vendor Ref. Value";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(17, 15);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(43, 13);
            this.label60.TabIndex = 1;
            this.label60.Text = "Amount";
            // 
            // tb_c_bill_amount
            // 
            this.tb_c_bill_amount.Location = new System.Drawing.Point(116, 12);
            this.tb_c_bill_amount.Name = "tb_c_bill_amount";
            this.tb_c_bill_amount.Size = new System.Drawing.Size(100, 20);
            this.tb_c_bill_amount.TabIndex = 0;
            // 
            // tb_c_bill_vendorRefVal
            // 
            this.tb_c_bill_vendorRefVal.Location = new System.Drawing.Point(116, 38);
            this.tb_c_bill_vendorRefVal.Name = "tb_c_bill_vendorRefVal";
            this.tb_c_bill_vendorRefVal.Size = new System.Drawing.Size(100, 20);
            this.tb_c_bill_vendorRefVal.TabIndex = 0;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.panel9);
            this.tabPage4.Controls.Add(this.toolStrip_bill);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(737, 435);
            this.tabPage4.TabIndex = 2;
            this.tabPage4.Text = "Read";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.bt_r_bill_update);
            this.panel9.Controls.Add(this.txtMatchingField);
            this.panel9.Controls.Add(this.tb_r_bill_vendorRef);
            this.panel9.Controls.Add(this.tb_r_bill_balance);
            this.panel9.Controls.Add(this.tb_r_bill_totAmount);
            this.panel9.Controls.Add(this.label1);
            this.panel9.Controls.Add(this.listBox_bill);
            this.panel9.Controls.Add(this.label141);
            this.panel9.Controls.Add(this.label139);
            this.panel9.Controls.Add(this.label138);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel9.Location = new System.Drawing.Point(3, 28);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(731, 404);
            this.panel9.TabIndex = 8;
            // 
            // bt_r_bill_update
            // 
            this.bt_r_bill_update.Location = new System.Drawing.Point(206, 165);
            this.bt_r_bill_update.Name = "bt_r_bill_update";
            this.bt_r_bill_update.Size = new System.Drawing.Size(75, 23);
            this.bt_r_bill_update.TabIndex = 18;
            this.bt_r_bill_update.Text = "Update";
            this.bt_r_bill_update.UseVisualStyleBackColor = true;
            this.bt_r_bill_update.Click += new System.EventHandler(this.button23_Click);
            // 
            // txtMatchingField
            // 
            this.txtMatchingField.Location = new System.Drawing.Point(271, 143);
            this.txtMatchingField.Name = "txtMatchingField";
            this.txtMatchingField.Size = new System.Drawing.Size(100, 20);
            this.txtMatchingField.TabIndex = 14;
            // 
            // tb_r_bill_vendorRef
            // 
            this.tb_r_bill_vendorRef.Location = new System.Drawing.Point(271, 117);
            this.tb_r_bill_vendorRef.Name = "tb_r_bill_vendorRef";
            this.tb_r_bill_vendorRef.Size = new System.Drawing.Size(100, 20);
            this.tb_r_bill_vendorRef.TabIndex = 14;
            // 
            // tb_r_bill_balance
            // 
            this.tb_r_bill_balance.Location = new System.Drawing.Point(271, 91);
            this.tb_r_bill_balance.Name = "tb_r_bill_balance";
            this.tb_r_bill_balance.Size = new System.Drawing.Size(100, 20);
            this.tb_r_bill_balance.TabIndex = 14;
            // 
            // tb_r_bill_totAmount
            // 
            this.tb_r_bill_totAmount.Location = new System.Drawing.Point(271, 65);
            this.tb_r_bill_totAmount.Name = "tb_r_bill_totAmount";
            this.tb_r_bill_totAmount.Size = new System.Drawing.Size(100, 20);
            this.tb_r_bill_totAmount.TabIndex = 14;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(144, 146);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 13);
            this.label1.TabIndex = 0;
            this.label1.Tag = "ex: 1;44;77;33";
            this.label1.Text = "Matching Field Id List";
            // 
            // listBox_bill
            // 
            this.listBox_bill.Dock = System.Windows.Forms.DockStyle.Left;
            this.listBox_bill.FormattingEnabled = true;
            this.listBox_bill.Location = new System.Drawing.Point(0, 0);
            this.listBox_bill.Name = "listBox_bill";
            this.listBox_bill.Size = new System.Drawing.Size(138, 404);
            this.listBox_bill.TabIndex = 1;
            this.listBox_bill.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // label141
            // 
            this.label141.AutoSize = true;
            this.label141.Location = new System.Drawing.Point(144, 120);
            this.label141.Name = "label141";
            this.label141.Size = new System.Drawing.Size(61, 13);
            this.label141.TabIndex = 0;
            this.label141.Text = "Vendor Ref";
            // 
            // label139
            // 
            this.label139.AutoSize = true;
            this.label139.Location = new System.Drawing.Point(144, 94);
            this.label139.Name = "label139";
            this.label139.Size = new System.Drawing.Size(46, 13);
            this.label139.TabIndex = 0;
            this.label139.Text = "Balance";
            // 
            // label138
            // 
            this.label138.AutoSize = true;
            this.label138.Location = new System.Drawing.Point(144, 68);
            this.label138.Name = "label138";
            this.label138.Size = new System.Drawing.Size(70, 13);
            this.label138.TabIndex = 0;
            this.label138.Text = "Total Amount";
            // 
            // toolStrip_bill
            // 
            this.toolStrip_bill.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip_bill.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bt_showall_bill,
            this.tb_b_bill_query,
            this.bt_b_bill_execute});
            this.toolStrip_bill.Location = new System.Drawing.Point(3, 3);
            this.toolStrip_bill.Name = "toolStrip_bill";
            this.toolStrip_bill.Size = new System.Drawing.Size(731, 25);
            this.toolStrip_bill.TabIndex = 9;
            this.toolStrip_bill.Text = "toolStrip1";
            // 
            // bt_showall_bill
            // 
            this.bt_showall_bill.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bt_showall_bill.Image = ((System.Drawing.Image)(resources.GetObject("bt_showall_bill.Image")));
            this.bt_showall_bill.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bt_showall_bill.Name = "bt_showall_bill";
            this.bt_showall_bill.Size = new System.Drawing.Size(57, 22);
            this.bt_showall_bill.Text = "Show All";
            this.bt_showall_bill.Click += new System.EventHandler(this.button_showAll_Click);
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.bt_d_bill_delete);
            this.tabPage5.Controls.Add(this.label110);
            this.tabPage5.Controls.Add(this.tb_d_bill_id);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(737, 435);
            this.tabPage5.TabIndex = 3;
            this.tabPage5.Text = "Delete";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // bt_d_bill_delete
            // 
            this.bt_d_bill_delete.Location = new System.Drawing.Point(282, 9);
            this.bt_d_bill_delete.Name = "bt_d_bill_delete";
            this.bt_d_bill_delete.Size = new System.Drawing.Size(76, 23);
            this.bt_d_bill_delete.TabIndex = 9;
            this.bt_d_bill_delete.Text = "Delete";
            this.bt_d_bill_delete.UseVisualStyleBackColor = true;
            this.bt_d_bill_delete.Click += new System.EventHandler(this.button7_Click);
            // 
            // label110
            // 
            this.label110.AutoSize = true;
            this.label110.Location = new System.Drawing.Point(6, 14);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(32, 13);
            this.label110.TabIndex = 8;
            this.label110.Text = "Bill Id";
            // 
            // tb_d_bill_id
            // 
            this.tb_d_bill_id.Location = new System.Drawing.Point(75, 11);
            this.tb_d_bill_id.Name = "tb_d_bill_id";
            this.tb_d_bill_id.Size = new System.Drawing.Size(201, 20);
            this.tb_d_bill_id.TabIndex = 7;
            // 
            // tab_Customer
            // 
            this.tab_Customer.Controls.Add(this.tabControl2);
            this.tab_Customer.Location = new System.Drawing.Point(4, 29);
            this.tab_Customer.Name = "tab_Customer";
            this.tab_Customer.Padding = new System.Windows.Forms.Padding(3);
            this.tab_Customer.Size = new System.Drawing.Size(751, 467);
            this.tab_Customer.TabIndex = 1;
            this.tab_Customer.Text = "Customer";
            this.tab_Customer.UseVisualStyleBackColor = true;
            // 
            // tabControl2
            // 
            this.tabControl2.Controls.Add(this.tabPage3);
            this.tabControl2.Controls.Add(this.tabPage6);
            this.tabControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl2.Location = new System.Drawing.Point(3, 3);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(745, 461);
            this.tabControl2.TabIndex = 2;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.label78);
            this.tabPage3.Controls.Add(this.tb_c_customer_note);
            this.tabPage3.Controls.Add(this.label64);
            this.tabPage3.Controls.Add(this.label70);
            this.tabPage3.Controls.Add(this.tb_c_customer_displayname);
            this.tabPage3.Controls.Add(this.tb_c_customer_familyname);
            this.tabPage3.Controls.Add(this.tb_c_customer_givenname);
            this.tabPage3.Controls.Add(this.bt_c_customer_create);
            this.tabPage3.Controls.Add(this.label62);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(737, 435);
            this.tabPage3.TabIndex = 0;
            this.tabPage3.Text = "Create";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Location = new System.Drawing.Point(17, 92);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(30, 13);
            this.label78.TabIndex = 16;
            this.label78.Text = "Note";
            // 
            // tb_c_customer_note
            // 
            this.tb_c_customer_note.Location = new System.Drawing.Point(116, 89);
            this.tb_c_customer_note.Name = "tb_c_customer_note";
            this.tb_c_customer_note.Size = new System.Drawing.Size(100, 20);
            this.tb_c_customer_note.TabIndex = 15;
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(17, 14);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(72, 13);
            this.label64.TabIndex = 7;
            this.label64.Text = "Display Name";
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Location = new System.Drawing.Point(17, 66);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(67, 13);
            this.label70.TabIndex = 12;
            this.label70.Text = "Family Name";
            // 
            // tb_c_customer_displayname
            // 
            this.tb_c_customer_displayname.Location = new System.Drawing.Point(116, 11);
            this.tb_c_customer_displayname.Name = "tb_c_customer_displayname";
            this.tb_c_customer_displayname.Size = new System.Drawing.Size(100, 20);
            this.tb_c_customer_displayname.TabIndex = 6;
            // 
            // tb_c_customer_familyname
            // 
            this.tb_c_customer_familyname.Location = new System.Drawing.Point(116, 63);
            this.tb_c_customer_familyname.Name = "tb_c_customer_familyname";
            this.tb_c_customer_familyname.Size = new System.Drawing.Size(100, 20);
            this.tb_c_customer_familyname.TabIndex = 11;
            // 
            // tb_c_customer_givenname
            // 
            this.tb_c_customer_givenname.Location = new System.Drawing.Point(116, 38);
            this.tb_c_customer_givenname.Name = "tb_c_customer_givenname";
            this.tb_c_customer_givenname.Size = new System.Drawing.Size(100, 20);
            this.tb_c_customer_givenname.TabIndex = 5;
            // 
            // bt_c_customer_create
            // 
            this.bt_c_customer_create.Location = new System.Drawing.Point(78, 115);
            this.bt_c_customer_create.Name = "bt_c_customer_create";
            this.bt_c_customer_create.Size = new System.Drawing.Size(75, 23);
            this.bt_c_customer_create.TabIndex = 9;
            this.bt_c_customer_create.Text = "Create";
            this.bt_c_customer_create.UseVisualStyleBackColor = true;
            this.bt_c_customer_create.Click += new System.EventHandler(this.button2_Click);
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(17, 40);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(66, 13);
            this.label62.TabIndex = 8;
            this.label62.Text = "Given Name";
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.panel1);
            this.tabPage6.Controls.Add(this.toolStrip_customer);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(737, 435);
            this.tabPage6.TabIndex = 1;
            this.tabPage6.Text = "Read";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.bt_r_customer_update);
            this.panel1.Controls.Add(this.tb_r_customer_notes);
            this.panel1.Controls.Add(this.label18);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.tb_r_customer_displayname);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.tb_r_customer_givenname);
            this.panel1.Controls.Add(this.listBox_customer);
            this.panel1.Controls.Add(this.tb_r_customer_familyname);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 28);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(731, 404);
            this.panel1.TabIndex = 3;
            // 
            // bt_r_customer_update
            // 
            this.bt_r_customer_update.Location = new System.Drawing.Point(212, 138);
            this.bt_r_customer_update.Name = "bt_r_customer_update";
            this.bt_r_customer_update.Size = new System.Drawing.Size(75, 23);
            this.bt_r_customer_update.TabIndex = 16;
            this.bt_r_customer_update.Text = "Save";
            this.bt_r_customer_update.UseVisualStyleBackColor = true;
            this.bt_r_customer_update.Click += new System.EventHandler(this.Customer_Update_Click);
            // 
            // tb_r_customer_notes
            // 
            this.tb_r_customer_notes.Location = new System.Drawing.Point(251, 88);
            this.tb_r_customer_notes.Name = "tb_r_customer_notes";
            this.tb_r_customer_notes.Size = new System.Drawing.Size(100, 20);
            this.tb_r_customer_notes.TabIndex = 15;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(160, 91);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(35, 13);
            this.label18.TabIndex = 5;
            this.label18.Text = "Notes";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(160, 65);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(72, 13);
            this.label17.TabIndex = 6;
            this.label17.Text = "Display Name";
            // 
            // tb_r_customer_displayname
            // 
            this.tb_r_customer_displayname.Location = new System.Drawing.Point(251, 62);
            this.tb_r_customer_displayname.Name = "tb_r_customer_displayname";
            this.tb_r_customer_displayname.Size = new System.Drawing.Size(100, 20);
            this.tb_r_customer_displayname.TabIndex = 11;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(160, 39);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Family Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(160, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Given Name";
            // 
            // tb_r_customer_givenname
            // 
            this.tb_r_customer_givenname.Location = new System.Drawing.Point(251, 10);
            this.tb_r_customer_givenname.Name = "tb_r_customer_givenname";
            this.tb_r_customer_givenname.Size = new System.Drawing.Size(100, 20);
            this.tb_r_customer_givenname.TabIndex = 6;
            // 
            // listBox_customer
            // 
            this.listBox_customer.Dock = System.Windows.Forms.DockStyle.Left;
            this.listBox_customer.FormattingEnabled = true;
            this.listBox_customer.Location = new System.Drawing.Point(0, 0);
            this.listBox_customer.Name = "listBox_customer";
            this.listBox_customer.Size = new System.Drawing.Size(120, 404);
            this.listBox_customer.TabIndex = 0;
            this.listBox_customer.SelectedIndexChanged += new System.EventHandler(this.listBox2_SelectedIndexChanged);
            // 
            // tb_r_customer_familyname
            // 
            this.tb_r_customer_familyname.Location = new System.Drawing.Point(251, 36);
            this.tb_r_customer_familyname.Name = "tb_r_customer_familyname";
            this.tb_r_customer_familyname.Size = new System.Drawing.Size(100, 20);
            this.tb_r_customer_familyname.TabIndex = 5;
            // 
            // toolStrip_customer
            // 
            this.toolStrip_customer.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip_customer.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bt_showall_customer,
            this.tb_b_customer_query,
            this.bt_b_customer_execute});
            this.toolStrip_customer.Location = new System.Drawing.Point(3, 3);
            this.toolStrip_customer.Name = "toolStrip_customer";
            this.toolStrip_customer.Size = new System.Drawing.Size(731, 25);
            this.toolStrip_customer.TabIndex = 4;
            this.toolStrip_customer.Text = "toolStrip2";
            // 
            // bt_showall_customer
            // 
            this.bt_showall_customer.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bt_showall_customer.Image = ((System.Drawing.Image)(resources.GetObject("bt_showall_customer.Image")));
            this.bt_showall_customer.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bt_showall_customer.Name = "bt_showall_customer";
            this.bt_showall_customer.Size = new System.Drawing.Size(54, 22);
            this.bt_showall_customer.Text = "ShowAll";
            this.bt_showall_customer.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // tab_Employee
            // 
            this.tab_Employee.Controls.Add(this.tabControl3);
            this.tab_Employee.Location = new System.Drawing.Point(4, 29);
            this.tab_Employee.Name = "tab_Employee";
            this.tab_Employee.Size = new System.Drawing.Size(751, 467);
            this.tab_Employee.TabIndex = 2;
            this.tab_Employee.Text = "Employee";
            this.tab_Employee.UseVisualStyleBackColor = true;
            // 
            // tabControl3
            // 
            this.tabControl3.Controls.Add(this.tabPage7);
            this.tabControl3.Controls.Add(this.tabPage8);
            this.tabControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl3.Location = new System.Drawing.Point(0, 0);
            this.tabControl3.Name = "tabControl3";
            this.tabControl3.SelectedIndex = 0;
            this.tabControl3.Size = new System.Drawing.Size(751, 467);
            this.tabControl3.TabIndex = 4;
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this.bt_c_employee_create);
            this.tabPage7.Controls.Add(this.label72);
            this.tabPage7.Controls.Add(this.label68);
            this.tabPage7.Controls.Add(this.tb_c_employee_givenname);
            this.tabPage7.Controls.Add(this.tb_c_employee_familyname);
            this.tabPage7.Location = new System.Drawing.Point(4, 22);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage7.Size = new System.Drawing.Size(743, 441);
            this.tabPage7.TabIndex = 0;
            this.tabPage7.Text = "Create";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // bt_c_employee_create
            // 
            this.bt_c_employee_create.Location = new System.Drawing.Point(70, 61);
            this.bt_c_employee_create.Name = "bt_c_employee_create";
            this.bt_c_employee_create.Size = new System.Drawing.Size(75, 23);
            this.bt_c_employee_create.TabIndex = 17;
            this.bt_c_employee_create.Text = "Create";
            this.bt_c_employee_create.UseVisualStyleBackColor = true;
            this.bt_c_employee_create.Click += new System.EventHandler(this.button3_Click);
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Location = new System.Drawing.Point(17, 12);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(66, 13);
            this.label72.TabIndex = 14;
            this.label72.Text = "Given Name";
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Location = new System.Drawing.Point(17, 38);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(67, 13);
            this.label68.TabIndex = 16;
            this.label68.Text = "Family Name";
            // 
            // tb_c_employee_givenname
            // 
            this.tb_c_employee_givenname.Location = new System.Drawing.Point(116, 10);
            this.tb_c_employee_givenname.Name = "tb_c_employee_givenname";
            this.tb_c_employee_givenname.Size = new System.Drawing.Size(100, 20);
            this.tb_c_employee_givenname.TabIndex = 13;
            // 
            // tb_c_employee_familyname
            // 
            this.tb_c_employee_familyname.Location = new System.Drawing.Point(116, 35);
            this.tb_c_employee_familyname.Name = "tb_c_employee_familyname";
            this.tb_c_employee_familyname.Size = new System.Drawing.Size(100, 20);
            this.tb_c_employee_familyname.TabIndex = 15;
            // 
            // tabPage8
            // 
            this.tabPage8.Controls.Add(this.panel8);
            this.tabPage8.Controls.Add(this.toolStrip_employee);
            this.tabPage8.Location = new System.Drawing.Point(4, 22);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage8.Size = new System.Drawing.Size(743, 441);
            this.tabPage8.TabIndex = 1;
            this.tabPage8.Text = "Read";
            this.tabPage8.UseVisualStyleBackColor = true;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.tb_r_employee_checkname);
            this.panel8.Controls.Add(this.tb_r_employee_familyname);
            this.panel8.Controls.Add(this.tb_r_employee_givenname);
            this.panel8.Controls.Add(this.bt_r_employee_update);
            this.panel8.Controls.Add(this.label29);
            this.panel8.Controls.Add(this.label33);
            this.panel8.Controls.Add(this.label34);
            this.panel8.Controls.Add(this.listBox_employee);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel8.Location = new System.Drawing.Point(3, 28);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(737, 410);
            this.panel8.TabIndex = 3;
            // 
            // tb_r_employee_checkname
            // 
            this.tb_r_employee_checkname.Location = new System.Drawing.Point(299, 74);
            this.tb_r_employee_checkname.Name = "tb_r_employee_checkname";
            this.tb_r_employee_checkname.Size = new System.Drawing.Size(100, 20);
            this.tb_r_employee_checkname.TabIndex = 10;
            // 
            // tb_r_employee_familyname
            // 
            this.tb_r_employee_familyname.Location = new System.Drawing.Point(299, 48);
            this.tb_r_employee_familyname.Name = "tb_r_employee_familyname";
            this.tb_r_employee_familyname.Size = new System.Drawing.Size(100, 20);
            this.tb_r_employee_familyname.TabIndex = 10;
            // 
            // tb_r_employee_givenname
            // 
            this.tb_r_employee_givenname.Location = new System.Drawing.Point(299, 22);
            this.tb_r_employee_givenname.Name = "tb_r_employee_givenname";
            this.tb_r_employee_givenname.Size = new System.Drawing.Size(100, 20);
            this.tb_r_employee_givenname.TabIndex = 10;
            // 
            // bt_r_employee_update
            // 
            this.bt_r_employee_update.Location = new System.Drawing.Point(224, 128);
            this.bt_r_employee_update.Name = "bt_r_employee_update";
            this.bt_r_employee_update.Size = new System.Drawing.Size(75, 23);
            this.bt_r_employee_update.TabIndex = 9;
            this.bt_r_employee_update.Text = "Update";
            this.bt_r_employee_update.UseVisualStyleBackColor = true;
            this.bt_r_employee_update.Click += new System.EventHandler(this.update_employee_Click);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(152, 77);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(108, 13);
            this.label29.TabIndex = 6;
            this.label29.Text = "Print on Check Name";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(152, 51);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(67, 13);
            this.label33.TabIndex = 7;
            this.label33.Text = "Family Name";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(152, 25);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(66, 13);
            this.label34.TabIndex = 8;
            this.label34.Text = "Given Name";
            // 
            // listBox_employee
            // 
            this.listBox_employee.Dock = System.Windows.Forms.DockStyle.Left;
            this.listBox_employee.FormattingEnabled = true;
            this.listBox_employee.Location = new System.Drawing.Point(0, 0);
            this.listBox_employee.Name = "listBox_employee";
            this.listBox_employee.Size = new System.Drawing.Size(120, 410);
            this.listBox_employee.TabIndex = 0;
            this.listBox_employee.SelectedIndexChanged += new System.EventHandler(this.listBox_employee_SelectedIndexChanged);
            // 
            // toolStrip_employee
            // 
            this.toolStrip_employee.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip_employee.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bt_showall_employee,
            this.tb_b_employee_query,
            this.bt_b_employee_execute});
            this.toolStrip_employee.Location = new System.Drawing.Point(3, 3);
            this.toolStrip_employee.Name = "toolStrip_employee";
            this.toolStrip_employee.Size = new System.Drawing.Size(737, 25);
            this.toolStrip_employee.TabIndex = 0;
            this.toolStrip_employee.Text = "toolStrip3";
            // 
            // bt_showall_employee
            // 
            this.bt_showall_employee.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bt_showall_employee.Image = ((System.Drawing.Image)(resources.GetObject("bt_showall_employee.Image")));
            this.bt_showall_employee.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bt_showall_employee.Name = "bt_showall_employee";
            this.bt_showall_employee.Size = new System.Drawing.Size(57, 22);
            this.bt_showall_employee.Text = "Show All";
            this.bt_showall_employee.Click += new System.EventHandler(this.showAll_Employee_Click);
            // 
            // tab_Invoice
            // 
            this.tab_Invoice.Controls.Add(this.tabControl6);
            this.tab_Invoice.Location = new System.Drawing.Point(4, 29);
            this.tab_Invoice.Name = "tab_Invoice";
            this.tab_Invoice.Size = new System.Drawing.Size(751, 467);
            this.tab_Invoice.TabIndex = 3;
            this.tab_Invoice.Text = "Invoice";
            this.tab_Invoice.UseVisualStyleBackColor = true;
            // 
            // tabControl6
            // 
            this.tabControl6.Controls.Add(this.tabPage14);
            this.tabControl6.Controls.Add(this.tabPage15);
            this.tabControl6.Controls.Add(this.tabPage16);
            this.tabControl6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl6.Location = new System.Drawing.Point(0, 0);
            this.tabControl6.Name = "tabControl6";
            this.tabControl6.SelectedIndex = 0;
            this.tabControl6.Size = new System.Drawing.Size(751, 467);
            this.tabControl6.TabIndex = 2;
            // 
            // tabPage14
            // 
            this.tabPage14.Controls.Add(this.bt_c_invoice_create);
            this.tabPage14.Controls.Add(this.label5);
            this.tabPage14.Controls.Add(this.label9);
            this.tabPage14.Controls.Add(this.tb_c_invoice_amount);
            this.tabPage14.Controls.Add(this.label3);
            this.tabPage14.Controls.Add(this.tb_c_invoice_itemref);
            this.tabPage14.Controls.Add(this.tb_c_invoice_customerref);
            this.tabPage14.Location = new System.Drawing.Point(4, 22);
            this.tabPage14.Name = "tabPage14";
            this.tabPage14.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage14.Size = new System.Drawing.Size(743, 441);
            this.tabPage14.TabIndex = 0;
            this.tabPage14.Text = "Create";
            this.tabPage14.UseVisualStyleBackColor = true;
            // 
            // bt_c_invoice_create
            // 
            this.bt_c_invoice_create.Location = new System.Drawing.Point(74, 88);
            this.bt_c_invoice_create.Name = "bt_c_invoice_create";
            this.bt_c_invoice_create.Size = new System.Drawing.Size(75, 23);
            this.bt_c_invoice_create.TabIndex = 8;
            this.bt_c_invoice_create.Text = "Create";
            this.bt_c_invoice_create.UseVisualStyleBackColor = true;
            this.bt_c_invoice_create.Click += new System.EventHandler(this.button4_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(14, 13);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "Amount";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(13, 65);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(104, 13);
            this.label9.TabIndex = 7;
            this.label9.Text = "Customer Ref. Value";
            // 
            // tb_c_invoice_amount
            // 
            this.tb_c_invoice_amount.Location = new System.Drawing.Point(122, 10);
            this.tb_c_invoice_amount.Name = "tb_c_invoice_amount";
            this.tb_c_invoice_amount.Size = new System.Drawing.Size(100, 20);
            this.tb_c_invoice_amount.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(14, 39);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Item Ref. Value";
            // 
            // tb_c_invoice_itemref
            // 
            this.tb_c_invoice_itemref.Location = new System.Drawing.Point(122, 36);
            this.tb_c_invoice_itemref.Name = "tb_c_invoice_itemref";
            this.tb_c_invoice_itemref.Size = new System.Drawing.Size(100, 20);
            this.tb_c_invoice_itemref.TabIndex = 4;
            // 
            // tb_c_invoice_customerref
            // 
            this.tb_c_invoice_customerref.Location = new System.Drawing.Point(121, 62);
            this.tb_c_invoice_customerref.Name = "tb_c_invoice_customerref";
            this.tb_c_invoice_customerref.Size = new System.Drawing.Size(100, 20);
            this.tb_c_invoice_customerref.TabIndex = 4;
            // 
            // tabPage15
            // 
            this.tabPage15.Controls.Add(this.bt_r_invoice_update);
            this.tabPage15.Controls.Add(this.label36);
            this.tabPage15.Controls.Add(this.listBox_invoice);
            this.tabPage15.Controls.Add(this.label37);
            this.tabPage15.Controls.Add(this.toolstrip);
            this.tabPage15.Controls.Add(this.label38);
            this.tabPage15.Controls.Add(this.label57);
            this.tabPage15.Controls.Add(this.label39);
            this.tabPage15.Controls.Add(this.label56);
            this.tabPage15.Controls.Add(this.label40);
            this.tabPage15.Controls.Add(this.label55);
            this.tabPage15.Controls.Add(this.label41);
            this.tabPage15.Controls.Add(this.label54);
            this.tabPage15.Controls.Add(this.label42);
            this.tabPage15.Controls.Add(this.label53);
            this.tabPage15.Controls.Add(this.label43);
            this.tabPage15.Controls.Add(this.label52);
            this.tabPage15.Controls.Add(this.label44);
            this.tabPage15.Controls.Add(this.label51);
            this.tabPage15.Controls.Add(this.label45);
            this.tabPage15.Controls.Add(this.label50);
            this.tabPage15.Controls.Add(this.label46);
            this.tabPage15.Controls.Add(this.label49);
            this.tabPage15.Controls.Add(this.label47);
            this.tabPage15.Controls.Add(this.label48);
            this.tabPage15.Location = new System.Drawing.Point(4, 22);
            this.tabPage15.Name = "tabPage15";
            this.tabPage15.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage15.Size = new System.Drawing.Size(743, 441);
            this.tabPage15.TabIndex = 1;
            this.tabPage15.Text = "Read";
            this.tabPage15.UseVisualStyleBackColor = true;
            // 
            // bt_r_invoice_update
            // 
            this.bt_r_invoice_update.Location = new System.Drawing.Point(220, 335);
            this.bt_r_invoice_update.Name = "bt_r_invoice_update";
            this.bt_r_invoice_update.Size = new System.Drawing.Size(75, 23);
            this.bt_r_invoice_update.TabIndex = 2;
            this.bt_r_invoice_update.Text = "Update";
            this.bt_r_invoice_update.UseVisualStyleBackColor = true;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(282, 169);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(28, 13);
            this.label36.TabIndex = 0;
            this.label36.Text = "Text";
            // 
            // listBox_invoice
            // 
            this.listBox_invoice.Dock = System.Windows.Forms.DockStyle.Left;
            this.listBox_invoice.FormattingEnabled = true;
            this.listBox_invoice.Location = new System.Drawing.Point(3, 28);
            this.listBox_invoice.Name = "listBox_invoice";
            this.listBox_invoice.Size = new System.Drawing.Size(120, 410);
            this.listBox_invoice.TabIndex = 1;
            this.listBox_invoice.SelectedIndexChanged += new System.EventHandler(this.listBox_invoice_SelectedIndexChanged);
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(282, 296);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(28, 13);
            this.label37.TabIndex = 0;
            this.label37.Text = "Text";
            // 
            // toolstrip
            // 
            this.toolstrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolstrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bt_showall_invoice,
            this.tb_b_invoice_query,
            this.bt_b_invoice_execute});
            this.toolstrip.Location = new System.Drawing.Point(3, 3);
            this.toolstrip.Name = "toolstrip";
            this.toolstrip.Size = new System.Drawing.Size(737, 25);
            this.toolstrip.TabIndex = 0;
            this.toolstrip.Text = "toolStrip6";
            // 
            // bt_showall_invoice
            // 
            this.bt_showall_invoice.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bt_showall_invoice.Image = ((System.Drawing.Image)(resources.GetObject("bt_showall_invoice.Image")));
            this.bt_showall_invoice.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bt_showall_invoice.Name = "bt_showall_invoice";
            this.bt_showall_invoice.Size = new System.Drawing.Size(57, 22);
            this.bt_showall_invoice.Text = "Show All";
            this.bt_showall_invoice.Click += new System.EventHandler(this.showAll_Invoice_Click);
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(282, 143);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(28, 13);
            this.label38.TabIndex = 0;
            this.label38.Text = "Text";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(140, 46);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(66, 13);
            this.label57.TabIndex = 0;
            this.label57.Text = "Given Name";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(282, 272);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(28, 13);
            this.label39.TabIndex = 0;
            this.label39.Text = "Text";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(140, 69);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(67, 13);
            this.label56.TabIndex = 0;
            this.label56.Text = "Family Name";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(282, 119);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(28, 13);
            this.label40.TabIndex = 0;
            this.label40.Text = "Text";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(140, 199);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(46, 13);
            this.label55.TabIndex = 0;
            this.label55.Text = "Balance";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(282, 246);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(28, 13);
            this.label41.TabIndex = 0;
            this.label41.Text = "Text";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(140, 93);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(82, 13);
            this.label54.TabIndex = 0;
            this.label54.Text = "Company Name";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(282, 93);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(28, 13);
            this.label42.TabIndex = 0;
            this.label42.Text = "Text";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(140, 222);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(91, 13);
            this.label53.TabIndex = 0;
            this.label53.Text = "Balance With Job";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(282, 222);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(28, 13);
            this.label43.TabIndex = 0;
            this.label43.Text = "Text";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(140, 119);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(108, 13);
            this.label52.TabIndex = 0;
            this.label52.Text = "Print on Check Name";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(282, 69);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(28, 13);
            this.label44.TabIndex = 0;
            this.label44.Text = "Text";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(140, 246);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(89, 13);
            this.label51.TabIndex = 0;
            this.label51.Text = "Shipping Address";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(282, 199);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(28, 13);
            this.label45.TabIndex = 0;
            this.label45.Text = "Text";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(140, 143);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(73, 13);
            this.label50.TabIndex = 0;
            this.label50.Text = "Email Address";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(282, 46);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(28, 13);
            this.label46.TabIndex = 0;
            this.label46.Text = "Text";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(140, 272);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(61, 13);
            this.label49.TabIndex = 0;
            this.label49.Text = "Bill Address";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(140, 296);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(45, 13);
            this.label47.TabIndex = 0;
            this.label47.Text = "Taxable";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(140, 169);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(73, 13);
            this.label48.TabIndex = 0;
            this.label48.Text = "Tax Code Ref";
            // 
            // tabPage16
            // 
            this.tabPage16.Controls.Add(this.tb_d_invoice_id);
            this.tabPage16.Controls.Add(this.bt_d_invoice_delete);
            this.tabPage16.Controls.Add(this.label116);
            this.tabPage16.Location = new System.Drawing.Point(4, 22);
            this.tabPage16.Name = "tabPage16";
            this.tabPage16.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage16.Size = new System.Drawing.Size(743, 441);
            this.tabPage16.TabIndex = 2;
            this.tabPage16.Text = "Delete";
            this.tabPage16.UseVisualStyleBackColor = true;
            // 
            // tb_d_invoice_id
            // 
            this.tb_d_invoice_id.Location = new System.Drawing.Point(81, 15);
            this.tb_d_invoice_id.Name = "tb_d_invoice_id";
            this.tb_d_invoice_id.Size = new System.Drawing.Size(201, 20);
            this.tb_d_invoice_id.TabIndex = 4;
            // 
            // bt_d_invoice_delete
            // 
            this.bt_d_invoice_delete.Location = new System.Drawing.Point(289, 15);
            this.bt_d_invoice_delete.Name = "bt_d_invoice_delete";
            this.bt_d_invoice_delete.Size = new System.Drawing.Size(75, 23);
            this.bt_d_invoice_delete.TabIndex = 6;
            this.bt_d_invoice_delete.Text = "Delete";
            this.bt_d_invoice_delete.UseVisualStyleBackColor = true;
            this.bt_d_invoice_delete.Click += new System.EventHandler(this.button10_Click);
            // 
            // label116
            // 
            this.label116.AutoSize = true;
            this.label116.Location = new System.Drawing.Point(12, 18);
            this.label116.Name = "label116";
            this.label116.Size = new System.Drawing.Size(54, 13);
            this.label116.TabIndex = 5;
            this.label116.Text = "Invoice Id";
            // 
            // tab_Item
            // 
            this.tab_Item.Controls.Add(this.tabControl4);
            this.tab_Item.Location = new System.Drawing.Point(4, 29);
            this.tab_Item.Name = "tab_Item";
            this.tab_Item.Size = new System.Drawing.Size(751, 467);
            this.tab_Item.TabIndex = 4;
            this.tab_Item.Text = "Item";
            this.tab_Item.UseVisualStyleBackColor = true;
            // 
            // tabControl4
            // 
            this.tabControl4.Controls.Add(this.tabPage9);
            this.tabControl4.Controls.Add(this.tabPage10);
            this.tabControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl4.Location = new System.Drawing.Point(0, 0);
            this.tabControl4.Name = "tabControl4";
            this.tabControl4.SelectedIndex = 0;
            this.tabControl4.Size = new System.Drawing.Size(751, 467);
            this.tabControl4.TabIndex = 2;
            // 
            // tabPage9
            // 
            this.tabPage9.Controls.Add(this.dt_c_item_date);
            this.tabPage9.Controls.Add(this.bt_c_item_create);
            this.tabPage9.Controls.Add(this.label30);
            this.tabPage9.Controls.Add(this.label26);
            this.tabPage9.Controls.Add(this.tb_c_item_name);
            this.tabPage9.Controls.Add(this.label24);
            this.tabPage9.Controls.Add(this.label13);
            this.tabPage9.Controls.Add(this.label20);
            this.tabPage9.Controls.Add(this.tb_c_item_assest);
            this.tabPage9.Controls.Add(this.tb_c_item_expense);
            this.tabPage9.Controls.Add(this.tb_c_item_income);
            this.tabPage9.Location = new System.Drawing.Point(4, 22);
            this.tabPage9.Name = "tabPage9";
            this.tabPage9.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage9.Size = new System.Drawing.Size(743, 441);
            this.tabPage9.TabIndex = 0;
            this.tabPage9.Text = "Create";
            this.tabPage9.UseVisualStyleBackColor = true;
            // 
            // dt_c_item_date
            // 
            this.dt_c_item_date.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dt_c_item_date.Location = new System.Drawing.Point(146, 116);
            this.dt_c_item_date.Name = "dt_c_item_date";
            this.dt_c_item_date.Size = new System.Drawing.Size(102, 20);
            this.dt_c_item_date.TabIndex = 37;
            // 
            // bt_c_item_create
            // 
            this.bt_c_item_create.Location = new System.Drawing.Point(113, 142);
            this.bt_c_item_create.Name = "bt_c_item_create";
            this.bt_c_item_create.Size = new System.Drawing.Size(75, 23);
            this.bt_c_item_create.TabIndex = 36;
            this.bt_c_item_create.Text = "Create";
            this.bt_c_item_create.UseVisualStyleBackColor = true;
            this.bt_c_item_create.Click += new System.EventHandler(this.button5_Click);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(17, 116);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(30, 13);
            this.label30.TabIndex = 31;
            this.label30.Text = "Date";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(16, 92);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(111, 13);
            this.label26.TabIndex = 32;
            this.label26.Text = "Assest Account Value";
            // 
            // tb_c_item_name
            // 
            this.tb_c_item_name.Location = new System.Drawing.Point(145, 10);
            this.tb_c_item_name.Name = "tb_c_item_name";
            this.tb_c_item_name.Size = new System.Drawing.Size(100, 20);
            this.tb_c_item_name.TabIndex = 30;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(16, 65);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(121, 13);
            this.label24.TabIndex = 33;
            this.label24.Text = "Expense Account Value";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(16, 13);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(35, 13);
            this.label13.TabIndex = 35;
            this.label13.Text = "Name";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(16, 40);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(115, 13);
            this.label20.TabIndex = 34;
            this.label20.Text = "Income Account Value";
            // 
            // tb_c_item_assest
            // 
            this.tb_c_item_assest.Location = new System.Drawing.Point(145, 89);
            this.tb_c_item_assest.Name = "tb_c_item_assest";
            this.tb_c_item_assest.Size = new System.Drawing.Size(100, 20);
            this.tb_c_item_assest.TabIndex = 27;
            // 
            // tb_c_item_expense
            // 
            this.tb_c_item_expense.Location = new System.Drawing.Point(145, 62);
            this.tb_c_item_expense.Name = "tb_c_item_expense";
            this.tb_c_item_expense.Size = new System.Drawing.Size(100, 20);
            this.tb_c_item_expense.TabIndex = 28;
            // 
            // tb_c_item_income
            // 
            this.tb_c_item_income.Location = new System.Drawing.Point(145, 37);
            this.tb_c_item_income.Name = "tb_c_item_income";
            this.tb_c_item_income.Size = new System.Drawing.Size(100, 20);
            this.tb_c_item_income.TabIndex = 29;
            // 
            // tabPage10
            // 
            this.tabPage10.Controls.Add(this.panel7);
            this.tabPage10.Controls.Add(this.toolStrip_item);
            this.tabPage10.Location = new System.Drawing.Point(4, 22);
            this.tabPage10.Name = "tabPage10";
            this.tabPage10.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage10.Size = new System.Drawing.Size(743, 441);
            this.tabPage10.TabIndex = 1;
            this.tabPage10.Text = "Read";
            this.tabPage10.UseVisualStyleBackColor = true;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.tb_r_item_assest);
            this.panel7.Controls.Add(this.tb_r_item_expense);
            this.panel7.Controls.Add(this.tb_r_item_income);
            this.panel7.Controls.Add(this.tb_r_item_name);
            this.panel7.Controls.Add(this.bt_r_item_update);
            this.panel7.Controls.Add(this.label75);
            this.panel7.Controls.Add(this.label77);
            this.panel7.Controls.Add(this.label79);
            this.panel7.Controls.Add(this.label80);
            this.panel7.Controls.Add(this.listBox_Item);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Location = new System.Drawing.Point(3, 28);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(737, 410);
            this.panel7.TabIndex = 1;
            // 
            // tb_r_item_assest
            // 
            this.tb_r_item_assest.Location = new System.Drawing.Point(358, 120);
            this.tb_r_item_assest.Name = "tb_r_item_assest";
            this.tb_r_item_assest.Size = new System.Drawing.Size(100, 20);
            this.tb_r_item_assest.TabIndex = 14;
            // 
            // tb_r_item_expense
            // 
            this.tb_r_item_expense.Location = new System.Drawing.Point(358, 94);
            this.tb_r_item_expense.Name = "tb_r_item_expense";
            this.tb_r_item_expense.Size = new System.Drawing.Size(100, 20);
            this.tb_r_item_expense.TabIndex = 14;
            // 
            // tb_r_item_income
            // 
            this.tb_r_item_income.Location = new System.Drawing.Point(358, 68);
            this.tb_r_item_income.Name = "tb_r_item_income";
            this.tb_r_item_income.Size = new System.Drawing.Size(100, 20);
            this.tb_r_item_income.TabIndex = 14;
            // 
            // tb_r_item_name
            // 
            this.tb_r_item_name.Location = new System.Drawing.Point(358, 42);
            this.tb_r_item_name.Name = "tb_r_item_name";
            this.tb_r_item_name.Size = new System.Drawing.Size(100, 20);
            this.tb_r_item_name.TabIndex = 14;
            // 
            // bt_r_item_update
            // 
            this.bt_r_item_update.Location = new System.Drawing.Point(245, 169);
            this.bt_r_item_update.Name = "bt_r_item_update";
            this.bt_r_item_update.Size = new System.Drawing.Size(75, 23);
            this.bt_r_item_update.TabIndex = 13;
            this.bt_r_item_update.Text = "Update";
            this.bt_r_item_update.UseVisualStyleBackColor = true;
            this.bt_r_item_update.Click += new System.EventHandler(this.update_item_Click);
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Location = new System.Drawing.Point(159, 123);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(111, 13);
            this.label75.TabIndex = 9;
            this.label75.Text = "Assest Account Value";
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Location = new System.Drawing.Point(159, 97);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(121, 13);
            this.label77.TabIndex = 10;
            this.label77.Text = "Expense Account Value";
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Location = new System.Drawing.Point(159, 71);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(115, 13);
            this.label79.TabIndex = 11;
            this.label79.Text = "Income Account Value";
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Location = new System.Drawing.Point(159, 45);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(35, 13);
            this.label80.TabIndex = 12;
            this.label80.Text = "Name";
            // 
            // listBox_Item
            // 
            this.listBox_Item.Dock = System.Windows.Forms.DockStyle.Left;
            this.listBox_Item.FormattingEnabled = true;
            this.listBox_Item.Location = new System.Drawing.Point(0, 0);
            this.listBox_Item.Name = "listBox_Item";
            this.listBox_Item.Size = new System.Drawing.Size(120, 410);
            this.listBox_Item.TabIndex = 0;
            this.listBox_Item.SelectedIndexChanged += new System.EventHandler(this.listBox_Item_SelectedIndexChanged);
            // 
            // toolStrip_item
            // 
            this.toolStrip_item.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip_item.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bt_showall_item,
            this.tb_b_item_query,
            this.bt_b_item_execute});
            this.toolStrip_item.Location = new System.Drawing.Point(3, 3);
            this.toolStrip_item.Name = "toolStrip_item";
            this.toolStrip_item.Size = new System.Drawing.Size(737, 25);
            this.toolStrip_item.TabIndex = 0;
            this.toolStrip_item.Text = "toolStrip4";
            // 
            // bt_showall_item
            // 
            this.bt_showall_item.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bt_showall_item.Image = ((System.Drawing.Image)(resources.GetObject("bt_showall_item.Image")));
            this.bt_showall_item.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bt_showall_item.Name = "bt_showall_item";
            this.bt_showall_item.Size = new System.Drawing.Size(57, 22);
            this.bt_showall_item.Text = "Show All";
            this.bt_showall_item.Click += new System.EventHandler(this.showAll_Items_Click);
            // 
            // tab_PurchaseOrder
            // 
            this.tab_PurchaseOrder.Controls.Add(this.tabControl5);
            this.tab_PurchaseOrder.Location = new System.Drawing.Point(4, 29);
            this.tab_PurchaseOrder.Name = "tab_PurchaseOrder";
            this.tab_PurchaseOrder.Size = new System.Drawing.Size(751, 467);
            this.tab_PurchaseOrder.TabIndex = 5;
            this.tab_PurchaseOrder.Text = "Purchase Order";
            this.tab_PurchaseOrder.UseVisualStyleBackColor = true;
            // 
            // tabControl5
            // 
            this.tabControl5.Controls.Add(this.tabPage11);
            this.tabControl5.Controls.Add(this.tabPage12);
            this.tabControl5.Controls.Add(this.tabPage13);
            this.tabControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl5.Location = new System.Drawing.Point(0, 0);
            this.tabControl5.Name = "tabControl5";
            this.tabControl5.SelectedIndex = 0;
            this.tabControl5.Size = new System.Drawing.Size(751, 467);
            this.tabControl5.TabIndex = 2;
            // 
            // tabPage11
            // 
            this.tabPage11.Controls.Add(this.label108);
            this.tabPage11.Controls.Add(this.label32);
            this.tabPage11.Controls.Add(this.tb_c_purchase_customer);
            this.tabPage11.Controls.Add(this.label31);
            this.tabPage11.Controls.Add(this.label107);
            this.tabPage11.Controls.Add(this.tb_c_purchase_vendorref);
            this.tabPage11.Controls.Add(this.tb_c_purchase_quantity);
            this.tabPage11.Controls.Add(this.tb_c_purchase_apaccount);
            this.tabPage11.Controls.Add(this.radio_c_purchase_notbillable);
            this.tabPage11.Controls.Add(this.label28);
            this.tabPage11.Controls.Add(this.radio_c_purchase_hasbilled);
            this.tabPage11.Controls.Add(this.bt_c_purchase_create);
            this.tabPage11.Controls.Add(this.radio_c_purchase_billable);
            this.tabPage11.Controls.Add(this.tb_c_purchase_amount);
            this.tabPage11.Controls.Add(this.label76);
            this.tabPage11.Controls.Add(this.tb_c_purchase_itemref);
            this.tabPage11.Controls.Add(this.tb_c_purchase_unitprice);
            this.tabPage11.Controls.Add(this.label74);
            this.tabPage11.Location = new System.Drawing.Point(4, 22);
            this.tabPage11.Name = "tabPage11";
            this.tabPage11.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage11.Size = new System.Drawing.Size(743, 441);
            this.tabPage11.TabIndex = 0;
            this.tabPage11.Text = "Create";
            this.tabPage11.UseVisualStyleBackColor = true;
            // 
            // label108
            // 
            this.label108.AutoSize = true;
            this.label108.Location = new System.Drawing.Point(10, 226);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(51, 13);
            this.label108.TabIndex = 31;
            this.label108.Text = "Customer";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(11, 78);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(43, 13);
            this.label32.TabIndex = 28;
            this.label32.Text = "Amount";
            // 
            // tb_c_purchase_customer
            // 
            this.tb_c_purchase_customer.Location = new System.Drawing.Point(139, 223);
            this.tb_c_purchase_customer.Name = "tb_c_purchase_customer";
            this.tb_c_purchase_customer.Size = new System.Drawing.Size(100, 20);
            this.tb_c_purchase_customer.TabIndex = 30;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(12, 22);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(94, 13);
            this.label31.TabIndex = 25;
            this.label31.Text = "Vendor Ref. Value";
            // 
            // label107
            // 
            this.label107.AutoSize = true;
            this.label107.Location = new System.Drawing.Point(10, 194);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(46, 13);
            this.label107.TabIndex = 31;
            this.label107.Text = "Qunatity";
            // 
            // tb_c_purchase_vendorref
            // 
            this.tb_c_purchase_vendorref.Location = new System.Drawing.Point(141, 19);
            this.tb_c_purchase_vendorref.Name = "tb_c_purchase_vendorref";
            this.tb_c_purchase_vendorref.Size = new System.Drawing.Size(100, 20);
            this.tb_c_purchase_vendorref.TabIndex = 23;
            // 
            // tb_c_purchase_quantity
            // 
            this.tb_c_purchase_quantity.Location = new System.Drawing.Point(139, 191);
            this.tb_c_purchase_quantity.Name = "tb_c_purchase_quantity";
            this.tb_c_purchase_quantity.Size = new System.Drawing.Size(100, 20);
            this.tb_c_purchase_quantity.TabIndex = 30;
            // 
            // tb_c_purchase_apaccount
            // 
            this.tb_c_purchase_apaccount.Location = new System.Drawing.Point(140, 48);
            this.tb_c_purchase_apaccount.Name = "tb_c_purchase_apaccount";
            this.tb_c_purchase_apaccount.Size = new System.Drawing.Size(100, 20);
            this.tb_c_purchase_apaccount.TabIndex = 22;
            // 
            // radio_c_purchase_notbillable
            // 
            this.radio_c_purchase_notbillable.AutoSize = true;
            this.radio_c_purchase_notbillable.Location = new System.Drawing.Point(161, 105);
            this.radio_c_purchase_notbillable.Name = "radio_c_purchase_notbillable";
            this.radio_c_purchase_notbillable.Size = new System.Drawing.Size(78, 17);
            this.radio_c_purchase_notbillable.TabIndex = 29;
            this.radio_c_purchase_notbillable.Text = "Not Billable";
            this.radio_c_purchase_notbillable.UseVisualStyleBackColor = true;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(11, 51);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(94, 13);
            this.label28.TabIndex = 24;
            this.label28.Text = "AP Account Value";
            // 
            // radio_c_purchase_hasbilled
            // 
            this.radio_c_purchase_hasbilled.AutoSize = true;
            this.radio_c_purchase_hasbilled.Location = new System.Drawing.Point(86, 105);
            this.radio_c_purchase_hasbilled.Name = "radio_c_purchase_hasbilled";
            this.radio_c_purchase_hasbilled.Size = new System.Drawing.Size(72, 17);
            this.radio_c_purchase_hasbilled.TabIndex = 29;
            this.radio_c_purchase_hasbilled.Text = "Has Billed";
            this.radio_c_purchase_hasbilled.UseVisualStyleBackColor = true;
            // 
            // bt_c_purchase_create
            // 
            this.bt_c_purchase_create.Location = new System.Drawing.Point(87, 247);
            this.bt_c_purchase_create.Name = "bt_c_purchase_create";
            this.bt_c_purchase_create.Size = new System.Drawing.Size(75, 23);
            this.bt_c_purchase_create.TabIndex = 26;
            this.bt_c_purchase_create.Text = "Create";
            this.bt_c_purchase_create.UseVisualStyleBackColor = true;
            this.bt_c_purchase_create.Click += new System.EventHandler(this.button6_Click);
            // 
            // radio_c_purchase_billable
            // 
            this.radio_c_purchase_billable.AutoSize = true;
            this.radio_c_purchase_billable.Checked = true;
            this.radio_c_purchase_billable.Location = new System.Drawing.Point(22, 105);
            this.radio_c_purchase_billable.Name = "radio_c_purchase_billable";
            this.radio_c_purchase_billable.Size = new System.Drawing.Size(58, 17);
            this.radio_c_purchase_billable.TabIndex = 29;
            this.radio_c_purchase_billable.TabStop = true;
            this.radio_c_purchase_billable.Text = "Billable";
            this.radio_c_purchase_billable.UseVisualStyleBackColor = true;
            // 
            // tb_c_purchase_amount
            // 
            this.tb_c_purchase_amount.Location = new System.Drawing.Point(140, 75);
            this.tb_c_purchase_amount.Name = "tb_c_purchase_amount";
            this.tb_c_purchase_amount.Size = new System.Drawing.Size(100, 20);
            this.tb_c_purchase_amount.TabIndex = 27;
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Location = new System.Drawing.Point(11, 165);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(53, 13);
            this.label76.TabIndex = 28;
            this.label76.Text = "Unit Price";
            // 
            // tb_c_purchase_itemref
            // 
            this.tb_c_purchase_itemref.Location = new System.Drawing.Point(140, 133);
            this.tb_c_purchase_itemref.Name = "tb_c_purchase_itemref";
            this.tb_c_purchase_itemref.Size = new System.Drawing.Size(100, 20);
            this.tb_c_purchase_itemref.TabIndex = 27;
            // 
            // tb_c_purchase_unitprice
            // 
            this.tb_c_purchase_unitprice.Location = new System.Drawing.Point(140, 162);
            this.tb_c_purchase_unitprice.Name = "tb_c_purchase_unitprice";
            this.tb_c_purchase_unitprice.Size = new System.Drawing.Size(100, 20);
            this.tb_c_purchase_unitprice.TabIndex = 27;
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Location = new System.Drawing.Point(11, 136);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(80, 13);
            this.label74.TabIndex = 28;
            this.label74.Text = "Item Reference";
            // 
            // tabPage12
            // 
            this.tabPage12.Controls.Add(this.bt_r_purchase_update);
            this.tabPage12.Controls.Add(this.label82);
            this.tabPage12.Controls.Add(this.listBox_purchase);
            this.tabPage12.Controls.Add(this.label83);
            this.tabPage12.Controls.Add(this.label84);
            this.tabPage12.Controls.Add(this.toolStrip_purchase);
            this.tabPage12.Controls.Add(this.label85);
            this.tabPage12.Controls.Add(this.label103);
            this.tabPage12.Controls.Add(this.label86);
            this.tabPage12.Controls.Add(this.label102);
            this.tabPage12.Controls.Add(this.label87);
            this.tabPage12.Controls.Add(this.label101);
            this.tabPage12.Controls.Add(this.label88);
            this.tabPage12.Controls.Add(this.label100);
            this.tabPage12.Controls.Add(this.label89);
            this.tabPage12.Controls.Add(this.label99);
            this.tabPage12.Controls.Add(this.label90);
            this.tabPage12.Controls.Add(this.label98);
            this.tabPage12.Controls.Add(this.label91);
            this.tabPage12.Controls.Add(this.label97);
            this.tabPage12.Controls.Add(this.label92);
            this.tabPage12.Controls.Add(this.label96);
            this.tabPage12.Controls.Add(this.label93);
            this.tabPage12.Controls.Add(this.label95);
            this.tabPage12.Controls.Add(this.label94);
            this.tabPage12.Location = new System.Drawing.Point(4, 22);
            this.tabPage12.Name = "tabPage12";
            this.tabPage12.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage12.Size = new System.Drawing.Size(743, 441);
            this.tabPage12.TabIndex = 1;
            this.tabPage12.Text = "Read";
            this.tabPage12.UseVisualStyleBackColor = true;
            // 
            // bt_r_purchase_update
            // 
            this.bt_r_purchase_update.Location = new System.Drawing.Point(261, 341);
            this.bt_r_purchase_update.Name = "bt_r_purchase_update";
            this.bt_r_purchase_update.Size = new System.Drawing.Size(75, 23);
            this.bt_r_purchase_update.TabIndex = 9;
            this.bt_r_purchase_update.Text = "Update";
            this.bt_r_purchase_update.UseVisualStyleBackColor = true;
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Location = new System.Drawing.Point(297, 172);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(28, 13);
            this.label82.TabIndex = 0;
            this.label82.Text = "Text";
            // 
            // listBox_purchase
            // 
            this.listBox_purchase.Dock = System.Windows.Forms.DockStyle.Left;
            this.listBox_purchase.FormattingEnabled = true;
            this.listBox_purchase.Location = new System.Drawing.Point(3, 28);
            this.listBox_purchase.Name = "listBox_purchase";
            this.listBox_purchase.Size = new System.Drawing.Size(120, 410);
            this.listBox_purchase.TabIndex = 8;
            this.listBox_purchase.SelectedIndexChanged += new System.EventHandler(this.listBox_purchase_SelectedIndexChanged);
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Location = new System.Drawing.Point(297, 299);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(28, 13);
            this.label83.TabIndex = 0;
            this.label83.Text = "Text";
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Location = new System.Drawing.Point(297, 146);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(28, 13);
            this.label84.TabIndex = 0;
            this.label84.Text = "Text";
            // 
            // toolStrip_purchase
            // 
            this.toolStrip_purchase.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip_purchase.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bt_showall_purc,
            this.tb_b_purchase_query,
            this.bt_b_purchase_execute});
            this.toolStrip_purchase.Location = new System.Drawing.Point(3, 3);
            this.toolStrip_purchase.Name = "toolStrip_purchase";
            this.toolStrip_purchase.Size = new System.Drawing.Size(737, 25);
            this.toolStrip_purchase.TabIndex = 0;
            this.toolStrip_purchase.Text = "toolStrip5";
            // 
            // bt_showall_purc
            // 
            this.bt_showall_purc.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bt_showall_purc.Image = ((System.Drawing.Image)(resources.GetObject("bt_showall_purc.Image")));
            this.bt_showall_purc.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bt_showall_purc.Name = "bt_showall_purc";
            this.bt_showall_purc.Size = new System.Drawing.Size(57, 22);
            this.bt_showall_purc.Text = "Show All";
            this.bt_showall_purc.Click += new System.EventHandler(this.showAll_Purchase_Click);
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Location = new System.Drawing.Point(297, 275);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(28, 13);
            this.label85.TabIndex = 0;
            this.label85.Text = "Text";
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.Location = new System.Drawing.Point(155, 49);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(66, 13);
            this.label103.TabIndex = 0;
            this.label103.Text = "Given Name";
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Location = new System.Drawing.Point(297, 122);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(28, 13);
            this.label86.TabIndex = 0;
            this.label86.Text = "Text";
            // 
            // label102
            // 
            this.label102.AutoSize = true;
            this.label102.Location = new System.Drawing.Point(155, 72);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(67, 13);
            this.label102.TabIndex = 0;
            this.label102.Text = "Family Name";
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Location = new System.Drawing.Point(297, 249);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(28, 13);
            this.label87.TabIndex = 0;
            this.label87.Text = "Text";
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.Location = new System.Drawing.Point(155, 202);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(46, 13);
            this.label101.TabIndex = 0;
            this.label101.Text = "Balance";
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Location = new System.Drawing.Point(297, 96);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(28, 13);
            this.label88.TabIndex = 0;
            this.label88.Text = "Text";
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.Location = new System.Drawing.Point(155, 96);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(82, 13);
            this.label100.TabIndex = 0;
            this.label100.Text = "Company Name";
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Location = new System.Drawing.Point(297, 225);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(28, 13);
            this.label89.TabIndex = 0;
            this.label89.Text = "Text";
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.Location = new System.Drawing.Point(155, 225);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(91, 13);
            this.label99.TabIndex = 0;
            this.label99.Text = "Balance With Job";
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Location = new System.Drawing.Point(297, 72);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(28, 13);
            this.label90.TabIndex = 0;
            this.label90.Text = "Text";
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.Location = new System.Drawing.Point(155, 122);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(108, 13);
            this.label98.TabIndex = 0;
            this.label98.Text = "Print on Check Name";
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Location = new System.Drawing.Point(297, 202);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(28, 13);
            this.label91.TabIndex = 0;
            this.label91.Text = "Text";
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.Location = new System.Drawing.Point(155, 249);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(89, 13);
            this.label97.TabIndex = 0;
            this.label97.Text = "Shipping Address";
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Location = new System.Drawing.Point(297, 49);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(28, 13);
            this.label92.TabIndex = 0;
            this.label92.Text = "Text";
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.Location = new System.Drawing.Point(155, 146);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(73, 13);
            this.label96.TabIndex = 0;
            this.label96.Text = "Email Address";
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Location = new System.Drawing.Point(155, 299);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(45, 13);
            this.label93.TabIndex = 0;
            this.label93.Text = "Taxable";
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Location = new System.Drawing.Point(155, 275);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(61, 13);
            this.label95.TabIndex = 0;
            this.label95.Text = "Bill Address";
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.Location = new System.Drawing.Point(155, 172);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(73, 13);
            this.label94.TabIndex = 0;
            this.label94.Text = "Tax Code Ref";
            // 
            // tabPage13
            // 
            this.tabPage13.Controls.Add(this.tb_d_purchase_id);
            this.tabPage13.Controls.Add(this.label118);
            this.tabPage13.Controls.Add(this.bt_d_purchase_delete);
            this.tabPage13.Location = new System.Drawing.Point(4, 22);
            this.tabPage13.Name = "tabPage13";
            this.tabPage13.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage13.Size = new System.Drawing.Size(743, 441);
            this.tabPage13.TabIndex = 2;
            this.tabPage13.Text = "Delete";
            this.tabPage13.UseVisualStyleBackColor = true;
            // 
            // tb_d_purchase_id
            // 
            this.tb_d_purchase_id.Location = new System.Drawing.Point(105, 12);
            this.tb_d_purchase_id.Name = "tb_d_purchase_id";
            this.tb_d_purchase_id.Size = new System.Drawing.Size(171, 20);
            this.tb_d_purchase_id.TabIndex = 4;
            // 
            // label118
            // 
            this.label118.AutoSize = true;
            this.label118.Location = new System.Drawing.Point(6, 15);
            this.label118.Name = "label118";
            this.label118.Size = new System.Drawing.Size(93, 13);
            this.label118.TabIndex = 5;
            this.label118.Text = "Purchase Order Id";
            // 
            // bt_d_purchase_delete
            // 
            this.bt_d_purchase_delete.Location = new System.Drawing.Point(283, 12);
            this.bt_d_purchase_delete.Name = "bt_d_purchase_delete";
            this.bt_d_purchase_delete.Size = new System.Drawing.Size(75, 23);
            this.bt_d_purchase_delete.TabIndex = 6;
            this.bt_d_purchase_delete.Text = "Delete";
            this.bt_d_purchase_delete.UseVisualStyleBackColor = true;
            this.bt_d_purchase_delete.Click += new System.EventHandler(this.button12_Click);
            // 
            // tb_b_bill_query
            // 
            this.tb_b_bill_query.AutoSize = false;
            this.tb_b_bill_query.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_b_bill_query.Name = "tb_b_bill_query";
            this.tb_b_bill_query.Size = new System.Drawing.Size(300, 23);
            // 
            // bt_b_bill_execute
            // 
            this.bt_b_bill_execute.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bt_b_bill_execute.Image = ((System.Drawing.Image)(resources.GetObject("bt_b_bill_execute.Image")));
            this.bt_b_bill_execute.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bt_b_bill_execute.Name = "bt_b_bill_execute";
            this.bt_b_bill_execute.Size = new System.Drawing.Size(51, 22);
            this.bt_b_bill_execute.Text = "Execute";
            this.bt_b_bill_execute.Click += new System.EventHandler(this.bt_b_bill_execute_Click);
            // 
            // tb_b_customer_query
            // 
            this.tb_b_customer_query.AutoSize = false;
            this.tb_b_customer_query.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_b_customer_query.Name = "tb_b_customer_query";
            this.tb_b_customer_query.Size = new System.Drawing.Size(300, 25);
            // 
            // bt_b_customer_execute
            // 
            this.bt_b_customer_execute.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bt_b_customer_execute.Image = ((System.Drawing.Image)(resources.GetObject("bt_b_customer_execute.Image")));
            this.bt_b_customer_execute.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bt_b_customer_execute.Name = "bt_b_customer_execute";
            this.bt_b_customer_execute.Size = new System.Drawing.Size(51, 22);
            this.bt_b_customer_execute.Text = "Execute";
            this.bt_b_customer_execute.Click += new System.EventHandler(this.bt_b_customer_execute_Click);
            // 
            // tb_b_employee_query
            // 
            this.tb_b_employee_query.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_b_employee_query.Name = "tb_b_employee_query";
            this.tb_b_employee_query.Size = new System.Drawing.Size(300, 25);
            // 
            // bt_b_employee_execute
            // 
            this.bt_b_employee_execute.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bt_b_employee_execute.Image = ((System.Drawing.Image)(resources.GetObject("bt_b_employee_execute.Image")));
            this.bt_b_employee_execute.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bt_b_employee_execute.Name = "bt_b_employee_execute";
            this.bt_b_employee_execute.Size = new System.Drawing.Size(51, 22);
            this.bt_b_employee_execute.Text = "Execute";
            this.bt_b_employee_execute.Click += new System.EventHandler(this.bt_b_employee_execute_Click);
            // 
            // tb_b_invoice_query
            // 
            this.tb_b_invoice_query.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_b_invoice_query.Name = "tb_b_invoice_query";
            this.tb_b_invoice_query.Size = new System.Drawing.Size(300, 25);
            // 
            // bt_b_invoice_execute
            // 
            this.bt_b_invoice_execute.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bt_b_invoice_execute.Image = ((System.Drawing.Image)(resources.GetObject("bt_b_invoice_execute.Image")));
            this.bt_b_invoice_execute.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bt_b_invoice_execute.Name = "bt_b_invoice_execute";
            this.bt_b_invoice_execute.Size = new System.Drawing.Size(51, 22);
            this.bt_b_invoice_execute.Text = "Execute";
            this.bt_b_invoice_execute.Click += new System.EventHandler(this.bt_b_invoice_execute_Click);
            // 
            // tb_b_item_query
            // 
            this.tb_b_item_query.AutoSize = false;
            this.tb_b_item_query.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_b_item_query.Name = "tb_b_item_query";
            this.tb_b_item_query.Size = new System.Drawing.Size(300, 23);
            // 
            // bt_b_item_execute
            // 
            this.bt_b_item_execute.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bt_b_item_execute.Image = ((System.Drawing.Image)(resources.GetObject("bt_b_item_execute.Image")));
            this.bt_b_item_execute.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bt_b_item_execute.Name = "bt_b_item_execute";
            this.bt_b_item_execute.Size = new System.Drawing.Size(51, 22);
            this.bt_b_item_execute.Text = "Execute";
            this.bt_b_item_execute.Click += new System.EventHandler(this.bt_b_item_execute_Click);
            // 
            // tb_b_purchase_query
            // 
            this.tb_b_purchase_query.AutoSize = false;
            this.tb_b_purchase_query.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_b_purchase_query.Name = "tb_b_purchase_query";
            this.tb_b_purchase_query.Size = new System.Drawing.Size(300, 23);
            // 
            // bt_b_purchase_execute
            // 
            this.bt_b_purchase_execute.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.bt_b_purchase_execute.Image = ((System.Drawing.Image)(resources.GetObject("bt_b_purchase_execute.Image")));
            this.bt_b_purchase_execute.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bt_b_purchase_execute.Name = "bt_b_purchase_execute";
            this.bt_b_purchase_execute.Size = new System.Drawing.Size(51, 22);
            this.bt_b_purchase_execute.Text = "Execute";
            this.bt_b_purchase_execute.Click += new System.EventHandler(this.bt_b_purchase_execute_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(759, 500);
            this.Controls.Add(this.readPurchase_result);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.readPurchase_result.ResumeLayout(false);
            this.tab_Bill.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.toolStrip_bill.ResumeLayout(false);
            this.toolStrip_bill.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            this.tab_Customer.ResumeLayout(false);
            this.tabControl2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tabPage6.ResumeLayout(false);
            this.tabPage6.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.toolStrip_customer.ResumeLayout(false);
            this.toolStrip_customer.PerformLayout();
            this.tab_Employee.ResumeLayout(false);
            this.tabControl3.ResumeLayout(false);
            this.tabPage7.ResumeLayout(false);
            this.tabPage7.PerformLayout();
            this.tabPage8.ResumeLayout(false);
            this.tabPage8.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.toolStrip_employee.ResumeLayout(false);
            this.toolStrip_employee.PerformLayout();
            this.tab_Invoice.ResumeLayout(false);
            this.tabControl6.ResumeLayout(false);
            this.tabPage14.ResumeLayout(false);
            this.tabPage14.PerformLayout();
            this.tabPage15.ResumeLayout(false);
            this.tabPage15.PerformLayout();
            this.toolstrip.ResumeLayout(false);
            this.toolstrip.PerformLayout();
            this.tabPage16.ResumeLayout(false);
            this.tabPage16.PerformLayout();
            this.tab_Item.ResumeLayout(false);
            this.tabControl4.ResumeLayout(false);
            this.tabPage9.ResumeLayout(false);
            this.tabPage9.PerformLayout();
            this.tabPage10.ResumeLayout(false);
            this.tabPage10.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.toolStrip_item.ResumeLayout(false);
            this.toolStrip_item.PerformLayout();
            this.tab_PurchaseOrder.ResumeLayout(false);
            this.tabControl5.ResumeLayout(false);
            this.tabPage11.ResumeLayout(false);
            this.tabPage11.PerformLayout();
            this.tabPage12.ResumeLayout(false);
            this.tabPage12.PerformLayout();
            this.toolStrip_purchase.ResumeLayout(false);
            this.toolStrip_purchase.PerformLayout();
            this.tabPage13.ResumeLayout(false);
            this.tabPage13.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl readPurchase_result;
        private System.Windows.Forms.TabPage tab_Bill;
        private System.Windows.Forms.TabPage tab_Customer;
        private System.Windows.Forms.TabPage tab_Employee;
        private System.Windows.Forms.TabPage tab_Invoice;
        private System.Windows.Forms.TabPage tab_Item;
        private System.Windows.Forms.TabPage tab_PurchaseOrder;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.Label label92;
        private System.Windows.Forms.Label label98;
        private System.Windows.Forms.Label label100;
        private System.Windows.Forms.Label label102;
        private System.Windows.Forms.Label label103;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.Label label93;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.Label label95;
        private System.Windows.Forms.Label label96;
        private System.Windows.Forms.Label label97;
        private System.Windows.Forms.Label label99;
        private System.Windows.Forms.Label label101;
        private System.Windows.Forms.Button bt_c_bill_create;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.TextBox tb_c_bill_vendorRefVal;
        private System.Windows.Forms.TextBox tb_c_bill_amount;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.TextBox tb_c_customer_note;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.TextBox tb_c_customer_familyname;
        private System.Windows.Forms.Button bt_c_customer_create;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.TextBox tb_c_customer_givenname;
        private System.Windows.Forms.TextBox tb_c_customer_displayname;
        private System.Windows.Forms.Button bt_c_employee_create;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.TextBox tb_c_employee_familyname;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.TextBox tb_c_employee_givenname;
        private System.Windows.Forms.Button bt_c_purchase_create;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox tb_c_purchase_apaccount;
        private System.Windows.Forms.TextBox tb_c_purchase_vendorref;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox tb_c_purchase_amount;
        private System.Windows.Forms.RadioButton radio_c_purchase_notbillable;
        private System.Windows.Forms.RadioButton radio_c_purchase_hasbilled;
        private System.Windows.Forms.RadioButton radio_c_purchase_billable;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.TextBox tb_c_purchase_itemref;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.TextBox tb_c_purchase_unitprice;
        private System.Windows.Forms.Label label107;
        private System.Windows.Forms.TextBox tb_c_purchase_quantity;
        private System.Windows.Forms.Label label108;
        private System.Windows.Forms.TextBox tb_c_purchase_customer;
        private System.Windows.Forms.Button bt_d_bill_delete;
        private System.Windows.Forms.Label label110;
        private System.Windows.Forms.TextBox tb_d_bill_id;
        private System.Windows.Forms.TextBox tb_d_invoice_id;
        private System.Windows.Forms.Button bt_d_invoice_delete;
        private System.Windows.Forms.Label label116;
        private System.Windows.Forms.TextBox tb_d_purchase_id;
        private System.Windows.Forms.Button bt_d_purchase_delete;
        private System.Windows.Forms.Label label118;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TextBox tb_r_customer_notes;
        private System.Windows.Forms.TextBox tb_r_customer_displayname;
        private System.Windows.Forms.TextBox tb_r_customer_givenname;
        private System.Windows.Forms.TextBox tb_r_customer_familyname;
        private System.Windows.Forms.ToolStrip toolStrip_bill;
        private System.Windows.Forms.ToolStripButton bt_showall_bill;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Label label141;
        private System.Windows.Forms.Label label139;
        private System.Windows.Forms.Label label138;
        private System.Windows.Forms.ListBox listBox_bill;
        private System.Windows.Forms.Button bt_r_bill_update;
        private System.Windows.Forms.TextBox tb_r_bill_totAmount;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.ToolStrip toolStrip_customer;
        private System.Windows.Forms.ToolStripButton bt_showall_customer;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListBox listBox_customer;
        private System.Windows.Forms.Button bt_r_customer_update;
        private System.Windows.Forms.TabControl tabControl3;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.TabPage tabPage8;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.ListBox listBox_employee;
        private System.Windows.Forms.ToolStrip toolStrip_employee;
        private System.Windows.Forms.ToolStripButton bt_showall_employee;
        private System.Windows.Forms.Button bt_r_employee_update;
        private System.Windows.Forms.TextBox tb_r_employee_checkname;
        private System.Windows.Forms.TextBox tb_r_employee_familyname;
        private System.Windows.Forms.TextBox tb_r_employee_givenname;
        private System.Windows.Forms.TabControl tabControl4;
        private System.Windows.Forms.TabPage tabPage9;
        private System.Windows.Forms.DateTimePicker dt_c_item_date;
        private System.Windows.Forms.Button bt_c_item_create;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox tb_c_item_name;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox tb_c_item_assest;
        private System.Windows.Forms.TextBox tb_c_item_expense;
        private System.Windows.Forms.TextBox tb_c_item_income;
        private System.Windows.Forms.TabPage tabPage10;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.TextBox tb_r_item_assest;
        private System.Windows.Forms.TextBox tb_r_item_expense;
        private System.Windows.Forms.TextBox tb_r_item_income;
        private System.Windows.Forms.TextBox tb_r_item_name;
        private System.Windows.Forms.Button bt_r_item_update;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.ListBox listBox_Item;
        private System.Windows.Forms.ToolStrip toolStrip_item;
        private System.Windows.Forms.ToolStripButton bt_showall_item;
        private System.Windows.Forms.TabControl tabControl5;
        private System.Windows.Forms.TabPage tabPage11;
        private System.Windows.Forms.TabPage tabPage12;
        private System.Windows.Forms.ListBox listBox_purchase;
        private System.Windows.Forms.ToolStrip toolStrip_purchase;
        private System.Windows.Forms.ToolStripButton bt_showall_purc;
        private System.Windows.Forms.TabPage tabPage13;
        private System.Windows.Forms.Button bt_r_purchase_update;
        private System.Windows.Forms.TabControl tabControl6;
        private System.Windows.Forms.TabPage tabPage14;
        private System.Windows.Forms.Button bt_c_invoice_create;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox tb_c_invoice_amount;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tb_c_invoice_itemref;
        private System.Windows.Forms.TextBox tb_c_invoice_customerref;
        private System.Windows.Forms.TabPage tabPage15;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.ListBox listBox_invoice;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.ToolStrip toolstrip;
        private System.Windows.Forms.ToolStripButton bt_showall_invoice;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.TabPage tabPage16;
        private System.Windows.Forms.TextBox tb_r_bill_vendorRef;
        private System.Windows.Forms.TextBox tb_r_bill_balance;
        private System.Windows.Forms.Button bt_r_invoice_update;
        private System.Windows.Forms.TextBox txtMatchingField;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolStripTextBox tb_b_bill_query;
        private System.Windows.Forms.ToolStripButton bt_b_bill_execute;
        private System.Windows.Forms.ToolStripTextBox tb_b_customer_query;
        private System.Windows.Forms.ToolStripButton bt_b_customer_execute;
        private System.Windows.Forms.ToolStripTextBox tb_b_employee_query;
        private System.Windows.Forms.ToolStripButton bt_b_employee_execute;
        private System.Windows.Forms.ToolStripTextBox tb_b_invoice_query;
        private System.Windows.Forms.ToolStripButton bt_b_invoice_execute;
        private System.Windows.Forms.ToolStripTextBox tb_b_item_query;
        private System.Windows.Forms.ToolStripButton bt_b_item_execute;
        private System.Windows.Forms.ToolStripTextBox tb_b_purchase_query;
        private System.Windows.Forms.ToolStripButton bt_b_purchase_execute;
    }
}

