﻿using Intuit.Ipp.Core;
using Intuit.Ipp.Data;
using Intuit.Ipp.LinqExtender.Ast;
using Intuit.Ipp.QueryFilter;
using Intuit.Ipp.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntuitApp.ClassFiles
{
    public class IntuitClass
    {
        public Intuit.Ipp.DataService.DataService service { get; set; }
        private String accesstoken = "";
        private String accesssecret = "";
        private String consumer = "";
        private String consumersecret = "";
        private String companyId = "";
        private String AppToken = "";
        private char[] splitchar = { ';' };
        private QueryService<Bill> billservices;
        private QueryService<Customer> customerservices;
        private QueryService<Employee> employeeservices;
        private QueryService<Invoice> invoiceservices;
        private QueryService<Item> itemservices;
        private QueryService<PurchaseOrder> purchaseorderservices;

        private Intuit.Ipp.DataService.DataService GetCurrentDataService(String filePath)
        {
            ReadFileData(filePath);
            OAuthRequestValidator rquest = new OAuthRequestValidator(accesstoken, accesssecret, consumer, consumersecret);
            ServiceContext cconext = new ServiceContext(AppToken, companyId, IntuitServicesType.QBO, rquest);
            cconext.IppConfiguration.BaseUrl.Qbo = "https://sandbox-quickbooks.api.intuit.com/";
            cconext.IppConfiguration.Message.Request.SerializationFormat = Intuit.Ipp.Core.Configuration.SerializationFormat.Json;
            cconext.IppConfiguration.Message.Response.SerializationFormat = Intuit.Ipp.Core.Configuration.SerializationFormat.Json;
            Intuit.Ipp.DataService.DataService mservice = new Intuit.Ipp.DataService.DataService(cconext);
            billservices=new QueryService<Bill>(cconext);
            customerservices = new QueryService<Customer>(cconext);
            employeeservices = new QueryService<Employee>(cconext);
            invoiceservices = new QueryService<Invoice>(cconext);
            itemservices = new QueryService<Item>(cconext);
            purchaseorderservices = new QueryService<PurchaseOrder>(cconext);
            return mservice;
        }
        private void ReadFileData(String filePath)
        {
            String[] AllKeys = getKeys(SingleLineRead(filePath));
            accesstoken = AllKeys[0];
            accesssecret = AllKeys[1];
            consumer = AllKeys[2];
            consumersecret = AllKeys[3];
            AppToken = AllKeys[4];
            companyId = AllKeys[5];

        }
        private String[] getKeys(String line)
        {
            return line.Split(splitchar, StringSplitOptions.RemoveEmptyEntries);
        }
        private String SingleLineRead(String filePath)
        {
            String KeyData = System.IO.File.ReadAllText(filePath);
            return KeyData;
        }
        



        public IntuitClass(String filePath)
        {
            service = GetCurrentDataService(filePath);
        }
        public Intuit.Ipp.Data.Bill GetBill(String Id)
        {
            Intuit.Ipp.Data.Bill dd = new Intuit.Ipp.Data.Bill();
            dd.Id = Id;
            try
            {
                return service.FindById<Intuit.Ipp.Data.Bill>(dd);
            }
            catch (Intuit.Ipp.Exception.IdsException ee)
            {
                return null;
            }
        }
        public Intuit.Ipp.Data.Customer GetCustomer(String Id)
        {
            Intuit.Ipp.Data.Customer dd = new Intuit.Ipp.Data.Customer();
            dd.Id = Id;
            try
            {
                return service.FindById<Intuit.Ipp.Data.Customer>(dd);
            }
            catch (Intuit.Ipp.Exception.IdsException ee)
            {
                return null;
            }
        }
        public Intuit.Ipp.Data.Employee GetEmployee(String Id)
        {
            Intuit.Ipp.Data.Employee dd = new Intuit.Ipp.Data.Employee();
            dd.Id = Id;
            try
            {
                return service.FindById<Intuit.Ipp.Data.Employee>(dd);
            }
            catch (Intuit.Ipp.Exception.IdsException ee)
            {
                return null;
            }
        }
        public Intuit.Ipp.Data.Invoice GetInvoice(String Id)
        {
            Intuit.Ipp.Data.Invoice dd = new Intuit.Ipp.Data.Invoice();
            dd.Id = Id;
            try
            {
                return service.FindById<Intuit.Ipp.Data.Invoice>(dd);
            }
            catch (Intuit.Ipp.Exception.IdsException ee)
            {
                return null;
            }
        }
        public Intuit.Ipp.Data.Item GetItem(String Id)
        {
            Intuit.Ipp.Data.Item dd = new Intuit.Ipp.Data.Item();
            dd.Id = Id;
            try
            {
                return service.FindById<Intuit.Ipp.Data.Item>(dd);
            }
            catch (Intuit.Ipp.Exception.IdsException ee)
            {
                return null;
            }
        }
        public Intuit.Ipp.Data.PurchaseOrder GetPurchaseOrder(String Id)
        {
            Intuit.Ipp.Data.PurchaseOrder dd = new Intuit.Ipp.Data.PurchaseOrder();
            dd.Id = Id;
            try
            {
                PurchaseOrder sdf=service.FindById<Intuit.Ipp.Data.PurchaseOrder>(dd);
                return sdf;
            }
            catch (Intuit.Ipp.Exception.IdsException ee)
            {
                return null;
            }
        }
        public Boolean DeleteBill(String Id)
        {
            Boolean result = false;
            try
            {
                Bill dele = service.Delete<Bill>(GetBill(Id));
                result = true;
            }
            catch (Intuit.Ipp.Exception.IdsException ee)
            {
                result = false;
            }
            return result;
        }
        public Boolean DeleteInvoice(String Id)
        {
            Boolean result = false;
            try
            {
                Invoice dele = service.Delete<Invoice>(GetInvoice(Id));
                result = true;
            }
            catch (Intuit.Ipp.Exception.IdsException ee)
            {
                result = false;
            }
            return result;
        }
        
        public Boolean DeletePurchaseOrder(String Id)
        {
            Boolean result = false;
            try
            {
                service.Delete<PurchaseOrder>(GetPurchaseOrder(Id));
                result = true;
            }
            catch (Intuit.Ipp.Exception.IdsException ee)
            {
                result = false;
            }
            return result;
        }
        public String CreateCutomer(String displayname,String givenName,String familyname,String note)
        {
            String result = "";
            Intuit.Ipp.Data.Customer newBill = new Intuit.Ipp.Data.Customer();
            newBill.GivenName = givenName;
            newBill.DisplayName = displayname;
            newBill.FamilyName = familyname;
            newBill.Notes = note;
            try
            {
                Customer getBill = service.Add<Customer>(newBill);
                 result= getBill.Id;
            }
            catch (Intuit.Ipp.Exception.IdsException ee)
            {
                result = "";
            }

            return result;
        }
        public String CreateEmployee(String givenName, String familyname)
        {
            String result = "";
            Intuit.Ipp.Data.Employee newBill = new Intuit.Ipp.Data.Employee();
            newBill.GivenName = givenName;
            newBill.FamilyName = familyname;
            try
            {
                Employee getBill = service.Add<Employee>(newBill);
                result = getBill.Id;
            }
            catch (Intuit.Ipp.Exception.IdsException ee)
            {
                result = "";
            }

            return result;
        }
        public String CreateItem(String mName, String IncomeAccountValue,String ExpenseValue,String AssestValue,DateTime date)
        {
            String result = "";
            ReferenceType assest = new ReferenceType();
            ReferenceType expense = new ReferenceType();
            ReferenceType income = new ReferenceType();
            assest.Value = AssestValue;
            expense.Value = ExpenseValue;
            income.Value = IncomeAccountValue;
            Intuit.Ipp.Data.Item newBill = new Intuit.Ipp.Data.Item();
            newBill.AssetAccountRef = assest;
            newBill.ExpenseAccountRef = expense;
            newBill.IncomeAccountRef = income;
            newBill.Name = mName;
            newBill.InvStartDate = date;
            try
            {
                Item getBill = service.Add<Item>(newBill);
                result = getBill.Id;
            }
            catch (Intuit.Ipp.Exception.IdsException ee)
            {
                result = "";
            }

            return result;
        }
        public List<Bill> GetAllBills()
        {
            List<Bill> AllBilss = new List<Bill>();
            String query = "select * from bill";
            try
            {
                AllBilss = billservices.ExecuteIdsQuery(query).ToList<Bill>();
            }
            catch (NotImplementedException ee)
            {
                
            }
            return AllBilss;
        }
        public List<Bill> GetAllBills(String query)
        {
            List<Bill> AllBilss = new List<Bill>();
            try
            {
                AllBilss = billservices.ExecuteIdsQuery(query).ToList<Bill>();
            }
            catch (NotImplementedException ee)
            {

            }
            return AllBilss;
        }
        public List<Customer> GetAllCustomers()
        {
            List<Customer> AllBilss = new List<Customer>();
            String query = "select * from customer";
            try
            {
                AllBilss = customerservices.ExecuteIdsQuery(query).ToList<Customer>();
            }
            catch (NotImplementedException ee)
            {
                
            }
            return AllBilss;
        }
        public List<Customer> GetAllCustomers(String query)
        {
            List<Customer> AllBilss = new List<Customer>();
            try
            {
                AllBilss = customerservices.ExecuteIdsQuery(query).ToList<Customer>();
            }
            catch (NotImplementedException ee)
            {

            }
            return AllBilss;
        }
        public List<Employee> GetAllEmployee()
        {
            List<Employee> AllBilss = new List<Employee>();
            String query = "select * from employee";
            try
            {
                AllBilss = employeeservices.ExecuteIdsQuery(query).ToList<Employee>();
            }
            catch (NotImplementedException ee)
            {

            }
            return AllBilss;
        }
        public List<Employee> GetAllEmployee(String query)
        {
            List<Employee> AllBilss = new List<Employee>();
            try
            {
                AllBilss = employeeservices.ExecuteIdsQuery(query).ToList<Employee>();
            }
            catch (NotImplementedException ee)
            {

            }
            return AllBilss;
        }
        public List<Item> GetAllItems()
        {
            List<Item> AllBilss = new List<Item>();
            String query = "select * from item";
            try
            {
                AllBilss = itemservices.ExecuteIdsQuery(query).ToList<Item>();
            }
            catch (NotImplementedException ee)
            {

            }
            return AllBilss;
        }
        public List<Item> GetAllItems(String query)
        {
            List<Item> AllBilss = new List<Item>();
            try
            {
                AllBilss = itemservices.ExecuteIdsQuery(query).ToList<Item>();
            }
            catch (NotImplementedException ee)
            {

            }
            return AllBilss;
        }
        public List<PurchaseOrder> GetAllPurchaseOrder()
        {
            List<PurchaseOrder> AllBilss = new List<PurchaseOrder>();
            String query = "select * from PurchaseOrder";
            try
            {
                AllBilss = purchaseorderservices.ExecuteIdsQuery(query).ToList<PurchaseOrder>();
            }
            catch (NotImplementedException ee)
            {

            }
            return AllBilss;
        }
        public List<PurchaseOrder> GetAllPurchaseOrder(String query)
        {
            List<PurchaseOrder> AllBilss = new List<PurchaseOrder>();
            try
            {
                AllBilss = purchaseorderservices.ExecuteIdsQuery(query).ToList<PurchaseOrder>();
            }
            catch (NotImplementedException ee)
            {

            }
            return AllBilss;
        }
        public List<Invoice> GetAllInvoices()
        {
            List<Invoice> AllBilss = new List<Invoice>();
            String query = "select * from Invoice";
            try
            {
                AllBilss = invoiceservices.ExecuteIdsQuery(query).ToList<Invoice>();
            }
            catch (NotImplementedException ee)
            {

            }
            return AllBilss;
        }
        public List<Invoice> GetAllInvoices(String query)
        {
            List<Invoice> AllBilss = new List<Invoice>();
            try
            {
                AllBilss = invoiceservices.ExecuteIdsQuery(query).ToList<Invoice>();
            }
            catch (NotImplementedException ee)
            {

            }
            return AllBilss;
        }

        public Boolean Update_Customer(Customer c)
        {
            Boolean result = false ;
            try
            {
                Customer cc=service.Update<Customer>(c);
                if(cc.Id!=null)
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
            }
            catch (Intuit.Ipp.Exception.IdsException ee)
            {

                result = false;
            }
            return result;
        }
        public Boolean Update_Employee(Employee c)
        {
            Boolean result = false;
            try
            {
                Employee cc = service.Update<Employee>(c);
                if (cc.Id != null)
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
            }
            catch (Intuit.Ipp.Exception.IdsException ee)
            {

                result = false;
            }
            return result;
        }
        public Boolean Update_Item(Item c)
        {
            Boolean result = false;
            try
            {
                Item cc = service.Update<Item>(c);
                if (cc.Id != null)
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
            }
            catch (Intuit.Ipp.Exception.IdsException ee)
            {

                result = false;
            }
            return result;
        }
        public Boolean Update_PurchaseOrder(PurchaseOrder c)
        {
            Boolean result = false;
            try
            {
                PurchaseOrder cc = service.Update<PurchaseOrder>(c);
                if (cc.Id != null)
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
            }
            catch (Intuit.Ipp.Exception.IdsException ee)
            {

                result = false;
            }
            return result;
        }
        public Boolean Update_Invoice(Invoice c)
        {
            Boolean result = false;
            try
            {
                Invoice cc = service.Update<Invoice>(c);
                if (cc.Id != null)
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
            }
            catch (Intuit.Ipp.Exception.IdsException ee)
            {

                result = false;
            }
            return result;
        }
        public Boolean Update_Bill(Bill c)
        {
            Boolean result = false;
            try
            {
                Bill cc = service.Update<Bill>(c);
                if (cc.Id != null)
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
            }
            catch (Intuit.Ipp.Exception.IdsException ee)
            {

                result = false;
            }
            return result;
        }
    }
}
